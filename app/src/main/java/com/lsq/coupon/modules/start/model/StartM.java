package com.lsq.coupon.modules.start.model;


import com.lsq.coupon.net.BaseInterfaceObject;

/**
 * Created by lsq on 2018/5/14.
 */

public interface StartM {

    //登录
    void login(String phone, String authCode, BaseInterfaceObject obj);

    //获得验证码
    void getCode(String phone, int type, BaseInterfaceObject obj);

    //登出登录
    void logintOut( BaseInterfaceObject obj);

    //修改密码
    void modifyPassWord(String newpwd, String oldpwd, BaseInterfaceObject lis);

    //注册
    void register(String phone, String inviteCode, String authCode, String password, BaseInterfaceObject obj);

    //忘记密码
    void forgetPassword(String phone, String authCode, String password,BaseInterfaceObject obj);
}
