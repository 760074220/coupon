package com.lsq.coupon.modules.my.list;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.lsq.coupon.R;
import com.lsq.coupon.adapter.IncomeListAdapter;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.modules.my._presenter.MyP;
import com.lsq.coupon.modules.my._presenter.MyPImp;
import com.lsq.coupon.modules.my._view.IncomeListV;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 收入榜单
 */
public class IncomeListActivity extends BaseActivity implements IncomeListV {


    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_join)
    TextView tvJoin;
    @BindView(R.id.tv_average_income)
    TextView tvAverageIncome;
    @BindView(R.id.tv_ranking)
    TextView tvRanking;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.iv_reward)
    ImageView ivReward;
    @BindView(R.id.money)
    TextView money;
    @BindView(R.id.recycler)
    RecyclerView recycler;


    private MyP incomeListP;

    private List<String> list;
    private IncomeListAdapter mAdapter;

    @Override
    protected String getPagerTitle() {
        return getString(R.string.income_list);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_income_list;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.white), true);
        incomeListP = new MyPImp(this, this, compositeDisposable);
        setAdapter();
    }

    private void setAdapter() {
        list = new ArrayList<>();
        list.add("1");
        list.add("1");
        list.add("1");
        list.add("1");
        list.add("1");
        list.add("1");
        list.add("1");
        mAdapter = new IncomeListAdapter(list);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(mAdapter);
        mAdapter.addFooterView(getFootView(this, recycler, "期待看到你榜上有名～"));

    }

    @Override
    protected void initData() {
        incomeListP.getIncomeList();
    }


    @OnClick(R.id.tv_invite)
    public void onViewClicked() {

    }

    public View getFootView(Context context, RecyclerView recyclerView, String title) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_income_list_hint_bottom, recyclerView, false);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        tvMsg.setText(title);
        return view;
    }

    @Override
    public void incomeListInfo() {

    }
}
