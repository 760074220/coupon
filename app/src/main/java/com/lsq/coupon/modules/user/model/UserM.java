package com.lsq.coupon.modules.user.model;

import com.lsq.coupon.net.BaseInterfaceObject;

/**
 * Created by lsq on 2019/3/13.
 */

public interface UserM {
    void getUserInfo(BaseInterfaceObject lis);
}
