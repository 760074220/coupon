package com.lsq.coupon.modules.start.presenter;

/**
 * Created by lsq on 2018/5/14.
 */

public interface StartP {
    //登录
    void login(String phone, String authCode);

    //获得验证码
    void getCode(String phone,int type);

    //登出登录
    void logintOut(String token);

    //修改密码
    void modifyPassWord(String newpwd, String oldpwd);

    //注册
    void register(String phone, String inviteCode, String authCode, String password);

    //忘记密码
    void forgetPassword(String phone, String authCode, String password);
}
