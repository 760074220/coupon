package com.lsq.coupon.modules.start.view;

/**
 * Created by lsq on 2018/5/14.
 * 登录界面需要做的事情
 */

public interface LoginV {
    void login();
}
