package com.lsq.coupon.modules.user.model;

import android.content.Context;

import com.lsq.coupon.net.BaseInterfaceObject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by lsq on 2019/3/13.
 */

public class UserMImp implements UserM {
    private Context context;
    private CompositeDisposable compositeDisposable;

    public UserMImp(Context context, CompositeDisposable compositeDisposable) {
        this.compositeDisposable = compositeDisposable;
        this.context = context;
    }

    @Override
    public void getUserInfo(BaseInterfaceObject lis) {
        lis.returnData(null);
    }
}
