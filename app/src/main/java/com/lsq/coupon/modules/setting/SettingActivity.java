package com.lsq.coupon.modules.setting;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.lsq.coupon.R;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.utils.CircleImageView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 设置
 */
public class SettingActivity extends BaseActivity {


    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_avatar)
    CircleImageView ivAvatar;
    @BindView(R.id.tv_alipay)
    TextView tvAlipay;
    @BindView(R.id.tv_wechat)
    TextView tvWechat;
    @BindView(R.id.tv_cache)
    TextView tvCache;

    @Override
    protected String getPagerTitle() {
        return getString(R.string.setting);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_setting;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.white), true);
    }

    @Override
    protected void initData() {

    }


    @OnClick({R.id.tv_modify_avator, R.id.tv_modify_nickname, R.id.r_alipay, R.id.r_wchat,
            R.id.r_modify_phone, R.id.r_modify_password, R.id.r_message_notice, R.id.tv_clear_cache, R.id.tv_login_out})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_modify_avator:
                break;
            case R.id.tv_modify_nickname:
                break;
            case R.id.r_alipay:
                break;
            case R.id.r_wchat:
                break;
            case R.id.r_modify_phone:
                break;
            case R.id.r_modify_password:
                break;
            case R.id.r_message_notice:
                break;
            case R.id.tv_clear_cache:
                break;
            case R.id.tv_login_out:
                break;
            default:
                break;
        }
    }
}
