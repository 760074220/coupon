package com.lsq.coupon.modules.start;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.lsq.coupon.R;
import com.lsq.coupon.base.ActivityManager;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.data.TabEntity;
import com.lsq.coupon.modules.home.MainActivity;
import com.lsq.coupon.modules.start.presenter.StartP;
import com.lsq.coupon.modules.start.presenter.StartPImp;
import com.lsq.coupon.modules.start.view.GetCodeV;
import com.lsq.coupon.modules.start.view.LoginV;
import com.lsq.coupon.utils.NotificationsUtils;
import com.lsq.coupon.utils.StringUtils;
import com.lsq.coupon.utils.ToastUtils;
import com.lsq.coupon.view.CountDownTimerUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by lsq on 2018/5/14.
 * 登录界面
 */
public class LoginActivity extends BaseActivity implements LoginV, GetCodeV {

    @BindView(R.id.toolbar_title)
    TextView toolBarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tab)
    CommonTabLayout tab;
    @BindView(R.id.ed_phone)
    EditText edPhone;
    @BindView(R.id.ed_code)
    EditText edCode;
    @BindView(R.id.ed_password)
    EditText edPassword;
    @BindView(R.id.tv_get_code)
    TextView tvGetCode;
    @BindView(R.id.ed_account)
    EditText edAccount;
    @BindView(R.id.l_account)
    LinearLayout lAccount;
    @BindView(R.id.l_phone)
    LinearLayout lPhone;

    private StartP loginPImp;
    private boolean phoneOrAccout = false;

    private String[] mTitles = {"账号密码登录", "手机验证码登录"};
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        toolbar.setNavigationIcon(null);
        boolean b = NotificationsUtils.isNotificationEnabled(this);
        if (!b) {
            NotificationsUtils.showDelDialog(this);
        }
        setTab();
    }

    @Override
    protected void initData() {
        loginPImp = new StartPImp(this, this, this, compositeDisposable);
    }

    @Override
    protected String getPagerTitle() {
        return getString(R.string.login);
    }


    private void setTab() {
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i]));
        }
        tab.setTabData(mTabEntities);

        tab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                if (position == 0 && phoneOrAccout == true) {
                    lAccount.setVisibility(View.VISIBLE);
                    lPhone.setVisibility(View.GONE);
                    phoneOrAccout = false;
                } else if (position == 1 && phoneOrAccout == false) {
                    lPhone.setVisibility(View.VISIBLE);
                    lAccount.setVisibility(View.GONE);
                    phoneOrAccout = true;
                }

            }

            @Override
            public void onTabReselect(int position) {

            }
        });
    }

    @OnClick({R.id.tv_login, R.id.tv_get_code, R.id.tv_register, R.id.iv_wechat, R.id.tv_fuwu, R.id.tv_yinsi, R.id.tv_forget_password})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_login:
//
//                if (TextUtils.equals(tvSelectLoginType.getText().toString(), "选择密码登录")) {
//                    loginPImp.login(edPhone.getText().toString(), edCode.getText().toString());
//                } else {
//                    loginPImp.login(edPhone.getText().toString(), edPassword.getText().toString());
//                }

                if (phoneOrAccout) {
                    //手机号登录
                    if (checkPhone(edPhone) || checkPassWord(edCode)) {
                        return;
                    }
                    loginPImp.login(edPhone.getText().toString(), edCode.getText().toString());
                } else {
                    //账号登录
                    if (checkPhone(edAccount) || checkPassWord(edPassword)) {
                        return;
                    }
                    loginPImp.login(edAccount.getText().toString(), edPassword.getText().toString());
                }
                break;
            case R.id.tv_get_code:
                if (checkPhone(edPhone)) {
                    return;
                }
                loginPImp.getCode(edPhone.getText().toString(), 2);
                break;
            case R.id.tv_register:
                ActivityManager.getInstance().startNextActivity(RegisterActivity.class);
                break;
            case R.id.tv_forget_password:
                ActivityManager.getInstance().startNextActivity(ForgetPasswordActivity.class);
                break;
            case R.id.iv_wechat:
                break;
            case R.id.tv_fuwu:
                break;
            case R.id.tv_yinsi:
                break;
            default:
                break;
        }
    }


    @Override
    public void login() {
        //RiderMainActivity
//        int role = UserManager.getInstance().getUserInfoBean().getRole();
//        String storeNo = UserManager.getInstance().getUserInfoBean().getStoreNo();
//        ActivityManager.getInstance().startNextActivity(role == 4
//                ? RiderMainActivity.class : role == 14 && TextUtils.isEmpty(storeNo)
//                ? SelectStoreActivity.class : role == 8
//                ? RiderLeaderMainActivity.class : ClerkMainActivity.class, true);
        ActivityManager.getInstance().startNextActivity(MainActivity.class, true);
    }

    @Override
    public void startCountdown() {
        CountDownTimerUtils mCountDownTimerUtils = new CountDownTimerUtils(tvGetCode, 60000, 1000);
        mCountDownTimerUtils.start();
    }

    public boolean checkPhone(EditText view) {
        boolean b = (view.getId() == R.id.ed_phone ? true : false);
        if (TextUtils.isEmpty(view.getText().toString())) {
            ToastUtils.showToast(getString(b ? R.string.login_hint_inout_phone : R.string.login_hint_inout_account));
            return true;
        }
        if (b & !StringUtils.isMobileNumber(view.getText().toString())) {
            ToastUtils.showToast(getString(R.string.login_toast_true_phone));
            return true;
        }
        return false;
    }

    public boolean checkPassWord(EditText view) {
        boolean b = (view.getId() == R.id.ed_code ? true : false);
        if (TextUtils.isEmpty(view.getText().toString())) {
            ToastUtils.showToast(getString(b ? R.string.login_hint_inout_code : R.string.login_hint_password));
            return true;
        }

        if (view.getText().toString().length() < (b ? 4 : 6)) {
            ToastUtils.showToast(getString(b ? R.string.login_toast_short_code : R.string.login_toast_short_password));
            return true;
        }
        return false;
    }
}
