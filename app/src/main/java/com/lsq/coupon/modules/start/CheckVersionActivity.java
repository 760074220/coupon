package com.lsq.coupon.modules.start;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.lsq.coupon.R;
import com.lsq.coupon.base.ActivityManager;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.data.VersionBean;
import com.lsq.coupon.utils.AccessRequestUtil;
import com.lsq.coupon.utils.DownloadUtil;
import com.lsq.coupon.utils.ToastUtils;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class CheckVersionActivity extends BaseActivity {

    @BindView(R.id.content_text)
    TextView content;
    @BindView(R.id.left_btn)
    TextView leftBtn;
    @BindView(R.id.right_btn)
    TextView rightBtn;
    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id._view)
    View _view;

    VersionBean versionBean;
//    private VersionMsgAdapter mAdapter;
//    private List<String> list;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_check_version;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
//        list = new ArrayList<>();
        versionBean = (VersionBean) getIntent().getSerializableExtra("versionBean");
        this.setFinishOnTouchOutside(false);
    }

    @Override
    protected void initData() {
        content.setText(versionBean.updateContent);
//        leftBtn.setText("否");
//        rightBtn.setText("是");
        if (versionBean.getDescriptionList() != null) {
//            list.addAll(versionBean.getDescriptionList());
        }
        if (versionBean.getForceUpdate() == 1) {
            leftBtn.setVisibility(View.GONE);
            _view.setVisibility(View.GONE);
        }

//        mAdapter = new VersionMsgAdapter(list);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @OnClick({R.id.left_btn, R.id.right_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.left_btn:
                if (versionBean.getForceUpdate() == 1) {
                    ActivityManager.getInstance().exitApp();
                } else {
//                    UserManager.getInstance().setNewVersion(String.valueOf(versionBean.getVersionNum()));
//                    UserManager.getInstance().setIsIgnoreVersion("1");
//                    UserManager.getInstance().setIgnoreVersion(versionBean.getVersionNum().toString());
                    finish();
                }
                break;
            case R.id.right_btn:
//                UserManager.getInstance().setIsIgnoreVersion("0");
                if (AccessRequestUtil.checkSDCardPermission(this)) {
                    showDownloadDialog(this, versionBean);
                }
                break;
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        showDownloadDialog(this, versionBean);
        super.onPermissionsGranted(requestCode, perms);
    }

    private void showDownloadDialog(final Context context, final VersionBean versionBean) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("下载新版本");
        progressDialog.setMessage("正在下载中,请稍后...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMax(100);
        progressDialog.setCancelable(false);
        progressDialog.show();
        //versionBean.apkPath
        DownloadUtil.get().download(versionBean.getPackageUrl(), "/", new DownloadUtil.OnDownloadListener() {
            @Override
            public void onDownloadSuccess(File file) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setAction(Intent.ACTION_VIEW);
//                String fileName = file.getAbsolutePath();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    Uri contentUri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", file);
                    intent.setDataAndType(contentUri, "application/vnd.android.package-archive");
                } else {
                    intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
                }
                startActivity(intent);
                ActivityManager.getInstance().exitApp();
            }

            @Override
            public void onDownloading(int progress) {
                progressDialog.setProgress(progress);
            }

            @Override
            public void onDownloadFailed() {
                ToastUtils.showToast("下载失败");
                if (versionBean.versionType.equals("1")) {
                    ActivityManager.getInstance().exitApp();
                } else {
                    progressDialog.dismiss();
                }
            }
        });
    }
}
