package com.lsq.coupon.modules.my._presenter;

/**
 * Created by lsq on 2019/3/14.
 */

public interface MyP {

    //查询我的收藏
    void searchMyCollection(int pageno, int pagesize);

    //确认收藏
    void sureCollection(String goodsId, String pic, int isTmall, String title, double price,
                        double orgPrice, double earnSum, int quanPrice);

    //取消收藏
    void cancelCollection(String goodsId);

    //我的粉丝统计
    void getFansCount();

    //我的粉丝列表
    void getFansList(int type, int listType, int pageno, int pagesize);

    //获取我的收益信息
    void getMyIncome();

    //提现
    void wihtDraw(double amout);

    //提现列表
    void getWithDrawList(int pageno, int pagesize);

    //获取用户排行榜
    void getIncomeList();

    //获取我的订单
    void getOrderList(Integer status, String time, int pageno, int pagesize);
}
