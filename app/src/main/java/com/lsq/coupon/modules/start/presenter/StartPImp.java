package com.lsq.coupon.modules.start.presenter;

import android.content.Context;

import com.lsq.coupon.base.ActivityManager;
import com.lsq.coupon.base.PreferenConstans;
import com.lsq.coupon.modules.start.model.StartMIpm;
import com.lsq.coupon.modules.start.view.ForgetPasswordV;
import com.lsq.coupon.modules.start.view.GetCodeV;
import com.lsq.coupon.modules.start.view.LoginV;
import com.lsq.coupon.modules.start.view.RegisterV;
import com.lsq.coupon.net.BaseInterfaceObject;
import com.lsq.coupon.utils.PreferencesUtil;
import com.lsq.coupon.utils.ToastUtils;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by lsq on 2018/5/14.
 */

public class StartPImp implements StartP {

    private StartMIpm startMIpm;
    private LoginV loginV;
    private GetCodeV getCodeV;
    private RegisterV registerV;
    private ForgetPasswordV forgetPasswordV;


    public StartPImp(LoginV loginV, GetCodeV getCodeV, Context mContext, CompositeDisposable compositeDisposable) {
        this.loginV = loginV;
        this.getCodeV = getCodeV;
        this.startMIpm = new StartMIpm(mContext, compositeDisposable);
    }

    public StartPImp(Context mContext, GetCodeV getCodeV, CompositeDisposable compositeDisposable) {
        this.getCodeV = getCodeV;
        this.startMIpm = new StartMIpm(mContext, compositeDisposable);
    }

    public StartPImp(Context mContext, RegisterV registerV, LoginV loginV, GetCodeV getCodeV, CompositeDisposable compositeDisposable) {
        this.registerV = registerV;
        this.getCodeV = getCodeV;
        this.loginV = loginV;
        this.startMIpm = new StartMIpm(mContext, compositeDisposable);
    }

    public StartPImp(Context mContext, ForgetPasswordV forgetPasswordV, GetCodeV getCodeV, CompositeDisposable compositeDisposable) {
        this.forgetPasswordV = forgetPasswordV;
        this.getCodeV = getCodeV;
        this.startMIpm = new StartMIpm(mContext, compositeDisposable);
    }


    @Override
    public void login(String phone, String authCode) {
        startMIpm.login(phone, authCode, new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {
                //储存数据
                PreferencesUtil.putString(PreferenConstans.ACCESSTOKEN, "10000");
                loginV.login();
            }
        });
    }

    @Override
    public void getCode(String phone, int type) {
        startMIpm.getCode(phone, type, new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {
                getCodeV.startCountdown();
            }
        });
    }

    @Override
    public void logintOut(String token) {

    }


    @Override
    public void modifyPassWord(String newpwd, String oldpwd) {
        startMIpm.modifyPassWord(oldpwd, newpwd, new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {
                ToastUtils.showToast("修改密码成功");
                ActivityManager.getInstance().startLogin();
            }
        });
    }

    @Override
    public void register(String phone, String inviteCode, String authCode, String password) {
        startMIpm.register(phone, inviteCode, authCode, password, new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {
                registerV.registerSuccess();
            }
        });
    }

    @Override
    public void forgetPassword(String phone, String authCode, String password) {
        startMIpm.forgetPassword(phone, authCode, password, new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {

            }
        });
    }
}
