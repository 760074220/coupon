package com.lsq.coupon.modules.start;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.lsq.coupon.R;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.modules.start.presenter.StartP;
import com.lsq.coupon.modules.start.presenter.StartPImp;
import com.lsq.coupon.modules.start.view.ForgetPasswordV;
import com.lsq.coupon.modules.start.view.GetCodeV;
import com.lsq.coupon.utils.StringUtils;
import com.lsq.coupon.utils.ToastUtils;
import com.lsq.coupon.view.CountDownTimerUtils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 忘记密码
 */
public class ForgetPasswordActivity extends BaseActivity implements ForgetPasswordV, GetCodeV {


    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ed_phone)
    EditText edPhone;
    @BindView(R.id.ed_code)
    EditText edCode;
    @BindView(R.id.tv_get_code)
    TextView tvGetCode;
    @BindView(R.id.ed_password)
    EditText edPassword;
    @BindView(R.id.ed_password_again)
    EditText edPasswordAgain;

    private StartP forgetP;

    @Override
    protected String getPagerTitle() {
        return getString(R.string.forget_password);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_forget_password;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        forgetP = new StartPImp(this, this, this, compositeDisposable);
    }

    @Override
    protected void initData() {

    }

    @OnClick({R.id.tv_get_code, R.id.tv_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_get_code:
                if (checkPhone()) {
                    return;
                }
                forgetP.getCode(edPhone.getText().toString(), 0);
                break;
            case R.id.tv_submit:
                if (checkPhone() || checkPassWord(edCode) || checkPassWord(edPassword) || checkPassWord(edPasswordAgain)) {
                    return;
                }
                forgetP.forgetPassword(edPhone.getText().toString(), edCode.getText().toString(), edPassword.getText().toString());
                break;
        }
    }

    @Override
    public void forgetSuccess() {
        finish();
    }

    @Override
    public void startCountdown() {
        CountDownTimerUtils mCountDownTimerUtils = new CountDownTimerUtils(tvGetCode, 60000, 1000);
        mCountDownTimerUtils.start();
    }

    public boolean checkPhone() {
        if (TextUtils.isEmpty(edPhone.getText().toString())) {
            ToastUtils.showToast(getString(R.string.login_hint_inout_phone));
            return true;
        }
        if (!StringUtils.isMobileNumber(edPhone.getText().toString())) {
            ToastUtils.showToast(getString(R.string.login_toast_true_phone));
            return true;
        }
        return false;
    }

    public boolean checkPassWord(EditText view) {
        boolean b = (view.getId() == R.id.ed_code ? true : false);
        if (TextUtils.isEmpty(view.getText().toString())) {
            ToastUtils.showToast(getString(b ? R.string.login_hint_inout_code : view.getId() == R.id.ed_password ? R.string.login_hint_password : R.string.login_hint_srue_password));
            return true;
        }

        if (view.getText().toString().length() < (b ? 4 : 6)) {
            ToastUtils.showToast(getString(b ? R.string.login_toast_short_code : view.getId() == R.id.ed_password ? R.string.login_toast_short_password : R.string.login_toast_srue_short_password));
            return true;
        }
        if (view.getId() == R.id.ed_password_again && !TextUtils.equals(edPassword.getText().toString(), edPasswordAgain.getText().toString())) {
            ToastUtils.showToast("2次密码不一致");
            return true;
        }
        return false;
    }
}
