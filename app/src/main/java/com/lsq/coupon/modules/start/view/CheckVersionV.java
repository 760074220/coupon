package com.lsq.coupon.modules.start.view;


import com.lsq.coupon.data.VersionBean;

/**
 * Created by lsq on 2018/8/24.
 */

public interface CheckVersionV {
    void checkVersion(VersionBean versionBean);
}
