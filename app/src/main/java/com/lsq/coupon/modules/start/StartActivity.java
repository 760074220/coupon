package com.lsq.coupon.modules.start;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.lsq.coupon.R;
import com.lsq.coupon.base.ActivityManager;
import com.lsq.coupon.base.MyApplication;
import com.lsq.coupon.base.UserManager;
import com.lsq.coupon.modules.home.MainActivity;
import com.lsq.coupon.utils.HideStatusBarUtils;
import com.lsq.coupon.utils.PrinterDisplayUtil;
import com.lsq.coupon.utils.VersionUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class StartActivity extends AppCompatActivity {
    private static final String TAG = "StartActivity";

    @BindView(R.id.img)
    ImageView img;

    AlphaAnimation alphaAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HideStatusBarUtils.hideStatusBar(this);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);
        MyApplication.flag = 0;
        ActivityManager.getInstance().push(this);
        alphaAnimation = (AlphaAnimation) AnimationUtils.loadAnimation(StartActivity.this, R.anim.alpha_start);
        img.startAnimation(alphaAnimation);
        VersionUtils.getSHA1(this);
        PrinterDisplayUtil.printerDisplay(this);
        Flowable.timer(1000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        ActivityManager.getInstance().startNextActivity(
                                UserManager.getInstance().isLogin()
                                        ? MainActivity.class : LoginActivity.class, true);
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        img.clearAnimation();
        alphaAnimation = null;
    }

    public static String sHA1(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            byte[] cert = info.signatures[0].toByteArray();
            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] publicKey = md.digest(cert);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < publicKey.length; i++) {
                String appendString = Integer.toHexString(0xFF & publicKey[i]).toUpperCase(Locale.US);
                if (appendString.length() == 1)
                    hexString.append("0");
                hexString.append(appendString);
                hexString.append(":");
            }
            String result = hexString.toString();
            return result.substring(0, result.length() - 1);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
