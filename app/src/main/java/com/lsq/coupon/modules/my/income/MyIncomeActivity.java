package com.lsq.coupon.modules.my.income;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.lsq.coupon.R;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.modules.my._presenter.MyP;
import com.lsq.coupon.modules.my._presenter.MyPImp;
import com.lsq.coupon.modules.my._view.MyIncomeV;
import com.lsq.coupon.modules.my._view.WithDrawV;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 我的收益
 */
public class MyIncomeActivity extends BaseActivity implements MyIncomeV, WithDrawV {


    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_last_month_income)
    TextView tvLastMonthIncome;
    @BindView(R.id.tv_this_month_income)
    TextView tvThisMonthIncome;
    @BindView(R.id.tv_today_pay_num)
    TextView tvTodayPayNum;
    @BindView(R.id.tv_today_income)
    TextView tvTodayIncome;
    @BindView(R.id.tv_toady_share)
    TextView tvToadyShare;
    @BindView(R.id.tv_yestoday_pay_num)
    TextView tvYestodayPayNum;
    @BindView(R.id.tv_yestoday_income)
    TextView tvYestodayIncome;
    @BindView(R.id.tv_yestoday_share)
    TextView tvYestodayShare;

    private MyP myIncome;

    @Override
    protected String getPagerTitle() {
        return getString(R.string.my_income);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_my_income;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.white), true);
        myIncome = new MyPImp(this, this, this, compositeDisposable);
    }

    @Override
    protected void initData() {
        myIncome.getMyIncome();
    }


    @OnClick({R.id.l_billing_details, R.id.l_withdraw_record})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.l_billing_details:
                break;
            case R.id.l_withdraw_record:
                break;
        }
    }

    @Override
    public void incomeInfo() {

    }

    @Override
    public void withDrawResult() {

    }
}
