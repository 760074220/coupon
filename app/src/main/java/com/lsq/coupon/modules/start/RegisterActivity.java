package com.lsq.coupon.modules.start;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.lsq.coupon.R;
import com.lsq.coupon.base.ActivityManager;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.modules.home.MainActivity;
import com.lsq.coupon.modules.start.presenter.StartP;
import com.lsq.coupon.modules.start.presenter.StartPImp;
import com.lsq.coupon.modules.start.view.GetCodeV;
import com.lsq.coupon.modules.start.view.LoginV;
import com.lsq.coupon.modules.start.view.RegisterV;
import com.lsq.coupon.utils.StringUtils;
import com.lsq.coupon.utils.ToastUtils;
import com.lsq.coupon.view.CountDownTimerUtils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 注册
 */
public class RegisterActivity extends BaseActivity implements RegisterV, GetCodeV, LoginV {


    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ed_invite_code)
    EditText edInviteCode;
    @BindView(R.id.ed_phone)
    EditText edPhone;
    @BindView(R.id.ed_code)
    EditText edCode;
    @BindView(R.id.tv_get_code)
    TextView tvGetCode;
    @BindView(R.id.ed_password)
    EditText edPassword;
    @BindView(R.id.ed_password_again)
    EditText edPasswordAgain;

    private StartP registerP;

    @Override
    protected String getPagerTitle() {
        return getString(R.string.register);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_register;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        registerP = new StartPImp(this, this, this,this, compositeDisposable);
    }

    @Override
    protected void initData() {

    }

    @OnClick({R.id.tv_get_code, R.id.tv_register, R.id.tv_fuwu, R.id.tv_yinsi})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_get_code:
                if (checkPhone(edPhone)) {
                    return;
                }
                registerP.getCode(edPhone.getText().toString(), 2);
                break;
            case R.id.tv_register:
                if (checkPhone(edPhone)) {
                    return;
                }
                if (checkPassWord(edCode)) {
                    return;
                }
                if (checkPassWord(edPassword)) {
                    return;
                }
                if (checkPassWord(edPasswordAgain)) {
                    return;
                }
                registerP.register(edPhone.getText().toString(), edInviteCode.getText().toString(), edCode.getText().toString(), edPassword.getText().toString());
                break;
            case R.id.tv_fuwu:
                break;
            case R.id.tv_yinsi:
                break;
        }
    }

    @Override
    public void registerSuccess() {
        registerP.login(edPhone.getText().toString(), edPassword.getText().toString());
    }

    @Override
    public void startCountdown() {
        CountDownTimerUtils mCountDownTimerUtils = new CountDownTimerUtils(tvGetCode, 60000, 1000);
        mCountDownTimerUtils.start();
    }

    public boolean checkPhone(EditText view) {
        boolean b = (view.getId() == R.id.ed_phone ? true : false);
        if (TextUtils.isEmpty(view.getText().toString())) {
            ToastUtils.showToast(getString(b ? R.string.login_hint_inout_phone : R.string.login_hint_inout_account));
            return true;
        }
        if (b & !StringUtils.isMobileNumber(view.getText().toString())) {
            ToastUtils.showToast(getString(R.string.login_toast_true_phone));
            return true;
        }
        return false;
    }

    public boolean checkPassWord(EditText view) {
        boolean b = (view.getId() == R.id.ed_code ? true : false);
        if (TextUtils.isEmpty(view.getText().toString())) {
            ToastUtils.showToast(getString(b ? R.string.login_hint_inout_code : view.getId() == R.id.ed_password ? R.string.login_hint_password : R.string.login_hint_srue_password));
            return true;
        }

        if (view.getText().toString().length() < (b ? 4 : 6)) {
            ToastUtils.showToast(getString(b ? R.string.login_toast_short_code : view.getId() == R.id.ed_password ? R.string.login_toast_short_password : R.string.login_toast_srue_short_password));
            return true;
        }
        if (view.getId() == R.id.ed_password_again && !TextUtils.equals(edPassword.getText().toString(), edPasswordAgain.getText().toString())) {
            ToastUtils.showToast("2次密码不一致");
            return true;
        }
        return false;
    }

    @Override
    public void login() {
        ActivityManager.getInstance().startNextActivity(MainActivity.class, true);
    }
}
