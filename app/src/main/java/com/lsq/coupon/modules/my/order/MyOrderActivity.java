package com.lsq.coupon.modules.my.order;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.lsq.coupon.R;
import com.lsq.coupon.adapter.MyOrderAdapter;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.modules.my._presenter.MyP;
import com.lsq.coupon.modules.my._presenter.MyPImp;
import com.lsq.coupon.modules.my._view.MyOrderV;
import com.lsq.coupon.utils.RecycleViewEmptyViewUtils;
import com.lsq.coupon.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 我的订单
 */
public class MyOrderActivity extends BaseActivity implements MyOrderV {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tab)
    TabLayout tab;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    private List<String> tabTitles;

    private MyP myOrderPImp;
    private Integer status = null;
    private String time = "";
    private int pageno = 1;
    private int pagesize = 20;

    private List<String> list;
    private MyOrderAdapter mAdapter;


    @Override
    protected String getPagerTitle() {
        return getString(R.string.my_order);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_my_order;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        myOrderPImp = new MyPImp(this, this, compositeDisposable);
        setAdapter();
        setTab();
    }

    @Override
    protected void initData() {
        myOrderPImp.getOrderList(status, time, pageno, pagesize);
    }

    private void setTab() {
        tabTitles = new ArrayList<>();
        tabTitles.add(getString(R.string.all_order));
        tabTitles.add(getString(R.string.is_pay_order));
        tabTitles.add(getString(R.string.is_settlement_order));
        tabTitles.add(getString(R.string.is_fail_order));
        for (int i = 0; i < tabTitles.size(); i++) {
            String title = tabTitles.get(i);
            View view = View.inflate(this, R.layout.tab_text_orange_black, null);
            TextView titleText = view.findViewById(R.id.title);
            titleText.setText(title);
//            titleText.setTextColor(R.drawable.selector_title_color_orange_black);
//            Drawable drawable = getResources().getDrawable(bottomTabIcons.getResourceId(i, 0));
//            drawable.setBounds(0, 0, DensityUtil.dp2px(this, 20), DensityUtil.dp2px(this, 20));
//            titleText.setCompoundDrawables(null, drawable, null, null);
            tab.addTab(tab.newTab().setCustomView(view));
        }

        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
//        TabLayoutUnderlineUtil.getInstance().setTabLine(tab,this,(int)getResources().getDimension(R.dimen.dp_14),(int)getResources().getDimension(R.dimen.dp_14));
    }

    private void setAdapter() {
        list = new ArrayList<>();
        list.add("1");
        list.add("1");
        mAdapter = new MyOrderAdapter(list);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(mAdapter);
        mAdapter.setEmptyView(new RecycleViewEmptyViewUtils().getView(this, recycler, "没有更多!"));
        /**
         * 上拉加载更多
         */
//        mAdapter.disableLoadMoreIfNotFullPage(recycler);
//        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
//            @Override
//            public void onLoadMoreRequested() {
//                if (listArrival.size() == pageNum * pageSize) {
//                    pageNum++;
//                    goodManagerPImp.getArrivalHistory( storeNo, edContact.getText().toString(), pageNum, pageSize);
//                } else {
//                    arrivalHistoryAdapter.loadMoreEnd(false);
//                }
//            }
//        }, recycler);

        /**
         * 下拉刷新
         */
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                initData();
            }
        });

    }

    @Override
    public void orderList() {
        ToastUtils.showToast("订单列表获取成功");
    }
}
