package com.lsq.coupon.modules.home;

import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.lsq.coupon.R;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.base.BaseFragment;
import com.lsq.coupon.modules.daily.DailyDiscountFragment;
import com.lsq.coupon.modules.share.ShareCommunityFragment;
import com.lsq.coupon.modules.user.UserFragment;
import com.lsq.coupon.utils.ToastUtils;

import java.util.Arrays;
import java.util.List;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.bottom_tab)
    TabLayout bottomTab;
    @BindArray(R.array.main_bottom_tab_text)
    TypedArray bottomTabTitles;
    @BindArray(R.array.main_bottom_tab_icon)
    TypedArray bottomTabIcons;
    List<String> tabTitles;


    @Override
    protected int getLayoutResId() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        return R.layout.activity_main;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
//        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.white), true);
//        StatusBarCompat.setLightStatusBar(this.getWindow(), true);
        tabTitles = Arrays.asList(getResources().getStringArray(R.array.main_bottom_tab_text));
        for (int i = 0; i < tabTitles.size(); i++) {
            String title = tabTitles.get(i);
            View view = View.inflate(this, R.layout.bottom_tab_view, null);
            TextView titleText = (TextView) view.findViewById(R.id.title);
            if (i != 2) {
                titleText.setText(title);
                Drawable drawable = getResources().getDrawable(bottomTabIcons.getResourceId(i, 0));
//            drawable.setBounds(0, 0, DensityUtil.dp2px(this, 20), DensityUtil.dp2px(this, 20));
                drawable.setBounds(0, 0, (int) getResources().getDimension(R.dimen.dp_30), (int) getResources().getDimension(R.dimen.dp_30));
                titleText.setCompoundDrawables(null, drawable, null, null);
            }
            bottomTab.addTab(bottomTab.newTab().setCustomView(view));
        }
        switchTab(R.string.home);
        bottomTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 2) {
                    return;
                }
                switchTab(bottomTabTitles.getResourceId(tab.getPosition(), 0));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void switchTab(int resId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        BaseFragment curFragment = null;
        switch (resId) {
            //这里要改
            case R.string.home:
                StatusBarCompat.setLightStatusBar(this.getWindow(), true);
                curFragment = HomeFragment.getInstance();
                break;
            case R.string.daily:
                StatusBarCompat.setLightStatusBar(this.getWindow(), true);
                curFragment = DailyDiscountFragment.getInstance();
                break;
            case R.string.share:
                StatusBarCompat.setLightStatusBar(this.getWindow(), true);
                curFragment = ShareCommunityFragment.getInstance();
                break;
            case R.string.user:
                StatusBarCompat.setLightStatusBar(this.getWindow(), false);
                curFragment = UserFragment.getInstance();
                break;
            default:
                break;
        }
        String tag = getString(resId);
        if (fragmentManager.findFragmentByTag(tag) != null) {
            transaction.show(fragmentManager.findFragmentByTag(tag));
        } else {
            transaction.add(R.id.container, curFragment, tag);
        }
        for (int i = 0; i < tabTitles.size(); i++) {
            if (resId != bottomTabTitles.getResourceId(i, 0)) {
                Fragment fragment = fragmentManager.findFragmentByTag(getString(bottomTabTitles.getResourceId(i, 0)));
                if (fragment != null) {
                    transaction.hide(fragment);
                }
            }
        }
        transaction.commitAllowingStateLoss();
        int currentPosition = tabTitles.indexOf(getResources().getString(resId));
//        bottomTab.getTabAt(currentPosition).select();
    }


    @Override
    protected void initData() {
    }

    @OnClick(R.id.iv_activity)
    public void onViewClicked() {
        ToastUtils.showToast("asdasd");
    }
}
