package com.lsq.coupon.modules.my.collection;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.lsq.coupon.R;
import com.lsq.coupon.adapter.LandscapeGoodAdapter;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.modules.my._presenter.MyP;
import com.lsq.coupon.modules.my._presenter.MyPImp;
import com.lsq.coupon.modules.my._view.CollectionStatusV;
import com.lsq.coupon.modules.my._view.SearchCollectionV;
import com.lsq.coupon.utils.RecycleViewEmptyViewUtils;
import com.lsq.coupon.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 我的收藏
 */
public class MyCollectionActivity extends BaseActivity implements CollectionStatusV, SearchCollectionV {


    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_baby)
    TextView tvBaby;
    @BindView(R.id.tv_collection)
    TextView tvCollection;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;
    private MyP myP;
    private int pageno = 1;
    private int pagesize = 20;

    private List<String> list;
    private LandscapeGoodAdapter mAdapter;

    @Override
    protected String getPagerTitle() {
        return getString(R.string.my_collection);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_my_collection;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.white), true);
        myP = new MyPImp(this, this, this, compositeDisposable);
        list = new ArrayList<>();
        tvBaby.setSelected(true);
        tvCollection.setSelected(false);
        setAdapter();
    }

    @Override
    protected void initData() {
        myP.searchMyCollection(pageno, pagesize);
    }


    @OnClick({R.id.tv_baby, R.id.tv_collection})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_baby:
                tvBaby.setSelected(true);
                tvCollection.setSelected(false);
                break;
            case R.id.tv_collection:
                tvBaby.setSelected(false);
                tvCollection.setSelected(true);
                break;
        }
    }

    private void setAdapter() {
        list.add("1");
        list.add("1");
        mAdapter = new LandscapeGoodAdapter(list);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(mAdapter);
        mAdapter.setEmptyView(new RecycleViewEmptyViewUtils().getView(this, recycler, "没有更多!"));
        /**
         * 上拉加载更多
         */
//        mAdapter.disableLoadMoreIfNotFullPage(recycler);
//        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
//            @Override
//            public void onLoadMoreRequested() {
//                if (listArrival.size() == pageNum * pageSize) {
//                    pageNum++;
//                    goodManagerPImp.getArrivalHistory( storeNo, edContact.getText().toString(), pageNum, pageSize);
//                } else {
//                    arrivalHistoryAdapter.loadMoreEnd(false);
//                }
//            }
//        }, recycler);

        /**
         * 下拉刷新
         */
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                initData();
            }
        });

    }

    @Override
    public void changeCollectionStatus() {

    }

    @Override
    public void myCollectionInfo() {
        ToastUtils.showToast("获取我的收藏");
    }
}
