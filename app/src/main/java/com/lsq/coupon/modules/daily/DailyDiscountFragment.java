package com.lsq.coupon.modules.daily;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lsq.coupon.R;
import com.lsq.coupon.adapter.LandscapeGoodAdapter;
import com.lsq.coupon.base.BaseFragment;
import com.lsq.coupon.utils.RecycleViewEmptyViewUtils;
import com.lsq.coupon.utils.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 每日折扣
 */
public class DailyDiscountFragment extends BaseFragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    private static DailyDiscountFragment fragment;
    @BindView(R.id.iv_red_view_one)
    ImageView ivRedViewOne;
    @BindView(R.id.tv_time_one)
    TextView tvTimeOne;
    @BindView(R.id.tv_rebate_one)
    TextView tvRebateOne;
    @BindView(R.id.tv_status_one)
    TextView tvStatusOne;
    @BindView(R.id.iv_red_view_two)
    ImageView ivRedViewTwo;
    @BindView(R.id.tv_time_two)
    TextView tvTimeTwo;
    @BindView(R.id.tv_rebate_two)
    TextView tvRebateTwo;
    @BindView(R.id.tv_status_two)
    TextView tvStatusTwo;
    @BindView(R.id.iv_red_view_three)
    ImageView ivRedViewThree;
    @BindView(R.id.tv_time_three)
    TextView tvTimeThree;
    @BindView(R.id.tv_rebate_three)
    TextView tvRebateThree;
    @BindView(R.id.tv_status_three)
    TextView tvStatusThree;
    @BindView(R.id.iv_red_view_four)
    ImageView ivRedViewFour;
    @BindView(R.id.tv_time_four)
    TextView tvTimeFour;
    @BindView(R.id.tv_rebate_four)
    TextView tvRebateFour;
    @BindView(R.id.tv_status_four)
    TextView tvStatusFour;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    private List<String> list;
    private LandscapeGoodAdapter mAdapter;

    public static DailyDiscountFragment getInstance() {
        if (fragment == null) {
            synchronized (DailyDiscountFragment.class) {
                if (fragment == null) {
                    fragment = new DailyDiscountFragment();
                }
            }
        }
        return fragment;
    }


    @Override
    public Toolbar getToolbar() {
        toolbarTitle.setText(getString(R.string.daily));
        return toolbar;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_daily_discount;
    }


    @Override
    protected void initData() {

    }

    @Override
    protected void init(Bundle savedInstanceState) {
        toolbar.setNavigationIcon(null);
        StatusBarUtil.changeStatusBarHeight(toolbar);
        setAdapter();
    }


    private void setAdapter() {
        list = new ArrayList<>();
        list.add("1");
        list.add("1");
        list.add("1");
        list.add("1");
        mAdapter = new LandscapeGoodAdapter(list);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler.setAdapter(mAdapter);
        mAdapter.setEmptyView(new RecycleViewEmptyViewUtils().getView(getActivity(), recycler, "没有更多!"));
        /**
         * 上拉加载更多
         */
//        mAdapter.disableLoadMoreIfNotFullPage(recycler);
//        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
//            @Override
//            public void onLoadMoreRequested() {
//                if (listArrival.size() == pageNum * pageSize) {
//                    pageNum++;
//                    goodManagerPImp.getArrivalHistory( storeNo, edContact.getText().toString(), pageNum, pageSize);
//                } else {
//                    arrivalHistoryAdapter.loadMoreEnd(false);
//                }
//            }
//        }, recycler);

        /**
         * 下拉刷新
         */
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                initData();
            }
        });
    }


    @OnClick({R.id.r_one, R.id.r_two, R.id.r_three, R.id.r_four})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.r_one:
                break;
            case R.id.r_two:
                break;
            case R.id.r_three:
                break;
            case R.id.r_four:
                break;
            default:
                break;
        }
    }
}
