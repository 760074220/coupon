package com.lsq.coupon.modules.user.presenter;

import android.content.Context;

import com.lsq.coupon.modules.user.model.UserM;
import com.lsq.coupon.modules.user.model.UserMImp;
import com.lsq.coupon.modules.user.view.UserV;
import com.lsq.coupon.net.BaseInterfaceObject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by lsq on 2019/3/13.
 */

public class UserPImp implements UserP {
    private UserM userM;
    private UserV userV;

    public UserPImp(Context context, UserV userV, CompositeDisposable compositeDisposable) {
        this.userM = new UserMImp(context, compositeDisposable);
        this.userV = userV;
    }

    @Override
    public void getUserInfo() {
        userM.getUserInfo(new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {
                userV.userInfo();
            }
        });
    }
}
