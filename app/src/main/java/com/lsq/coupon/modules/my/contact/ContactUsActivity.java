package com.lsq.coupon.modules.my.contact;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.lsq.coupon.R;
import com.lsq.coupon.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 联系我们
 */
public class ContactUsActivity extends BaseActivity {

    @BindView(R.id.tv_wechat_account)
    TextView tvWechatAccount;
    @BindView(R.id.tv_copy)
    TextView tvCopy;
    @BindView(R.id.iv_er_code)
    ImageView ivErCode;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_contact_us;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        StatusBarCompat.setTranslucent(this.getWindow(), true);
        StatusBarCompat.setLightStatusBar(this.getWindow(), true);
    }

    @Override
    protected void initData() {

    }


    @OnClick({R.id.tv_wechat_account, R.id.tv_copy})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_wechat_account:
                break;
            case R.id.tv_copy:
                break;
        }
    }
}
