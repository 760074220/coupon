package com.lsq.coupon.modules.my.invite;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.lsq.coupon.R;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.utils.transformer.ScaleInTransformer;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 我的邀请
 */
public class MyInviteActivity extends BaseActivity {


    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    private List<Integer> imgRes;


    @Override
    protected String getPagerTitle() {
        return getString(R.string.my_invite);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_my_invite;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.white), true);
        setViewPage();
    }


    @Override
    protected void initData() {

    }

    private void setViewPage() {
        imgRes = new ArrayList<>();
        imgRes.add(R.mipmap.pic_one);
        imgRes.add(R.mipmap.pic_two);
        imgRes.add(R.mipmap.pic_three);
        imgRes.add(R.mipmap.pic_four);
        //设置Page间间距
        mViewPager.setPageMargin(20);
        //设置缓存的页面数量
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(new PagerAdapter() {
            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                ImageView view = new ImageView(MyInviteActivity.this);
//                view.setImageResource(imgRes.get(position));
                view.setBackgroundResource(imgRes.get(position));
                container.addView(view);
                return view;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((View) object);
            }

            @Override
            public int getCount() {
                return imgRes.size();
            }

            @Override
            public boolean isViewFromObject(View view, Object o) {
                return view == o;
            }
        });

        mViewPager.setPageTransformer(true, new ScaleInTransformer(0.9f));
    }


    @OnClick({R.id.tv_copy, R.id.tv_share})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_copy:
                break;
            case R.id.tv_share:
                break;
        }
    }
}
