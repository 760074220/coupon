package com.lsq.coupon.modules.start;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.TextView;

import com.lsq.coupon.R;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.callback.MyTextWatcherListener;
import com.lsq.coupon.modules.start.presenter.StartP;
import com.lsq.coupon.utils.ToastUtils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 修改密码界面
 */
public class ModifyPassWordActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ed_old_password)
    EditText edOldPassword;
    @BindView(R.id.ed_new_password)
    EditText edNewPassword;
    @BindView(R.id.ed_again_password)
    EditText edAgainPassword;
    @BindView(R.id.tv_save)
    TextView tvSave;

    private StartP loginP;
    private boolean isCanSvae = false;


    private MyTextWatcherListener myTextWatcherListener;

    @Override
    protected String getPagerTitle() {
        return "修改密码";
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_modify_pass_word;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
//        loginP = new StartPImp(this, compositeDisposable);
        setListeren();
    }

    private void setListeren() {
        myTextWatcherListener = new MyTextWatcherListener() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if ((edOldPassword.getText().toString().length() > 5
                        && edNewPassword.getText().toString().length() > 5
                        && edAgainPassword.getText().toString().length() > 5)
                        && isCanSvae == false) {
                    isCanSvae = true;
//                    tvSave.setBackgroundResource(R.drawable.shape_clerk_white_solid_red_stroke);
//                    tvSave.setTextColor(getResources().getColor(R.color.red_FF5E5F));
                } else if ((edOldPassword.getText().toString().length() < 5
                        || edNewPassword.getText().toString().length() < 5
                        || edAgainPassword.getText().toString().length() < 5)
                        && isCanSvae == true) {
                    isCanSvae = false;
//                    tvSave.setBackgroundResource(R.drawable.shape_radio_gray);
                    tvSave.setTextColor(getResources().getColor(R.color.textColor99));
                }
            }
        };
        edOldPassword.addTextChangedListener(myTextWatcherListener);
        edNewPassword.addTextChangedListener(myTextWatcherListener);
        edAgainPassword.addTextChangedListener(myTextWatcherListener);
    }

    @Override
    protected void initData() {

    }

    @OnClick(R.id.tv_save)
    public void onViewClicked() {
        if (!isCanSvae) {
            ToastUtils.showToast("密码最少输入6位");
            return;
        }
        if (!edNewPassword.getText().toString().equals(edAgainPassword.getText().toString())) {
            ToastUtils.showToast("2次密码输入不一样");
            return;
        }
//        loginP.modifyPassWord(edOldPassword.getText().toString(), edNewPassword.getText().toString());
    }
}
