package com.lsq.coupon.modules.my.notice;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.lsq.coupon.R;
import com.lsq.coupon.adapter.NoticeAdapter;
import com.lsq.coupon.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 官方公告
 */
public class NoticeActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    private List<String> list;
    private NoticeAdapter mAdapter;

    @Override
    protected String getPagerTitle() {
        return getString(R.string.official_notice);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_notice;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.white), true);
        setAdapter();
    }

    @Override
    protected void initData() {

    }

    private void setAdapter() {

        list = new ArrayList<>();
        list.add("1");
        list.add("1");
        list.add("1");
        mAdapter = new NoticeAdapter(list);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(mAdapter);

        /**
         * 上拉加载更多
         */
//        mAdapter.disableLoadMoreIfNotFullPage(recycler);
//        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
//            @Override
//            public void onLoadMoreRequested() {
//                if (listArrival.size() == pageNum * pageSize) {
//                    pageNum++;
//                    goodManagerPImp.getArrivalHistory( storeNo, edContact.getText().toString(), pageNum, pageSize);
//                } else {
//                    arrivalHistoryAdapter.loadMoreEnd(false);
//                }
//            }
//        }, recycler);

        /**
         * 下拉刷新
         */
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                if (swipe.isRefreshing()) {
//                    //关闭刷新动画
//                    swipe.setRefreshing(false);
//                }
            }
        });
    }
}
