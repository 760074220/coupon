package com.lsq.coupon.modules.invite;

import android.os.Bundle;

import com.lsq.coupon.R;
import com.lsq.coupon.base.BaseActivity;

/**
 * 邀请活动
 */
public class InviteFunctionActivity extends BaseActivity {

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_invite_function;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {

    }
}
