package com.lsq.coupon.modules.good;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lsq.coupon.R;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.view.ImageTextView;

import butterknife.BindView;
import butterknife.OnClick;
import cn.bingoogolapple.bgabanner.BGABanner;

/**
 * 商品详情
 */
public class GoodDetailsActivity extends BaseActivity {


    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.banner)
    BGABanner banner;
    @BindView(R.id.iv_shipping)
    ImageView ivShipping;
    @BindView(R.id.tv_good_name)
    TextView tvGoodName;
    @BindView(R.id.tv_good_price)
    TextView tvGoodPrice;
    @BindView(R.id.tv_taobao_price)
    TextView tvTaobaoPrice;
    @BindView(R.id.tv_shipping)
    TextView tvShipping;
    @BindView(R.id.tv_sale_volume)
    TextView tvSaleVolume;
    @BindView(R.id.l_receive_coupon)
    LinearLayout lReceiveCoupon;
    @BindView(R.id.tv_look_good_details)
    TextView tvLookGoodDetails;
    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.itv_share)
    ImageTextView itvShare;

    @Override
    protected String getPagerTitle() {
        return "商品详情";
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_good_details;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {

    }


    @OnClick({R.id.l_receive_coupon, R.id.itv_home, R.id.itv_collection, R.id.itv_share, R.id.itv_coupon_buy})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.l_receive_coupon:
                break;
            case R.id.itv_home:
                break;
            case R.id.itv_collection:
                break;
            case R.id.itv_share:
                break;
            case R.id.itv_coupon_buy:
                break;
        }
    }
}
