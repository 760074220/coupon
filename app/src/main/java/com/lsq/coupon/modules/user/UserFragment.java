package com.lsq.coupon.modules.user;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lsq.coupon.R;
import com.lsq.coupon.adapter.UserFragmentAdapter;
import com.lsq.coupon.base.ActivityManager;
import com.lsq.coupon.base.BaseFragment;
import com.lsq.coupon.data.UserFragmentBean;
import com.lsq.coupon.modules.my.collection.MyCollectionActivity;
import com.lsq.coupon.modules.my.contact.ContactUsActivity;
import com.lsq.coupon.modules.my.fans.MyFansActivity;
import com.lsq.coupon.modules.my.feedback.FeedBackActivity;
import com.lsq.coupon.modules.my.income.MyIncomeActivity;
import com.lsq.coupon.modules.my.invite.MyInviteActivity;
import com.lsq.coupon.modules.my.list.IncomeListActivity;
import com.lsq.coupon.modules.my.notice.NoticeActivity;
import com.lsq.coupon.modules.my.order.MyOrderActivity;
import com.lsq.coupon.modules.my.question.QuestionActivity;
import com.lsq.coupon.modules.setting.SettingActivity;
import com.lsq.coupon.modules.user.presenter.UserP;
import com.lsq.coupon.modules.user.presenter.UserPImp;
import com.lsq.coupon.modules.user.view.UserV;
import com.lsq.coupon.utils.CircleImageView;
import com.lsq.coupon.utils.ClipboardManagerUtil;
import com.lsq.coupon.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class UserFragment extends BaseFragment implements UserV {
    private static UserFragment fragment;
    @BindView(R.id.iv_avatar)
    CircleImageView ivAvatar;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_super_member)
    TextView tvSuperMember;
    @BindView(R.id.tv_invite_code)
    TextView tvInviteCode;
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    @BindView(R.id.tv_month_estimate)
    TextView tvMonthEstimate;
    @BindView(R.id.tv_day_income)
    TextView tvDayIncome;
    @BindView(R.id.tv_last_month_settlement)
    TextView tvLastMonthSettlement;
    @BindView(R.id.tv_income)
    TextView tvIncome;
    @BindView(R.id.tv_order)
    TextView tvOrder;
    @BindView(R.id.tv_fans)
    TextView tvFans;
    @BindView(R.id.recycler)
    RecyclerView recycler;

    private UserP userP;

    private List<UserFragmentBean> list;
    private UserFragmentAdapter mAdapter;

    public static UserFragment getInstance() {
        if (fragment == null) {
            synchronized (UserFragment.class) {
                if (fragment == null) {
                    fragment = new UserFragment();
                }
            }
        }
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_user;
    }

    @Override
    protected void initData() {
        userP = new UserPImp(getActivity(), this, compositeDisposable);
        userP.getUserInfo();
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        list = new ArrayList<>();
        setAdapter();
    }

    private void setAdapter() {
        list.add(new UserFragmentBean(1, getString(R.string.my_collection), R.mipmap.ic_launcher));
        list.add(new UserFragmentBean(2, getString(R.string.success_case), R.mipmap.ic_launcher));
        list.add(new UserFragmentBean(3, getString(R.string.income_list), R.mipmap.ic_launcher));
        list.add(new UserFragmentBean(4, getString(R.string.usually_question), R.mipmap.ic_launcher));
        list.add(new UserFragmentBean(5, getString(R.string.feedback), R.mipmap.ic_launcher));
        list.add(new UserFragmentBean(6, getString(R.string.contact_service), R.mipmap.ic_launcher));
        list.add(new UserFragmentBean(7, getString(R.string.tiro_guide), R.mipmap.ic_launcher));
        list.add(new UserFragmentBean(8, getString(R.string.my_invite), R.mipmap.ic_launcher));
        list.add(new UserFragmentBean(9, getString(R.string.official_notice), R.mipmap.ic_launcher));
        mAdapter = new UserFragmentAdapter(list);
        recycler.setLayoutManager(new GridLayoutManager(context, 3));
        recycler.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                switch (position) {
                    case 0:
                        ActivityManager.getInstance().startNextActivity(MyCollectionActivity.class);
                        break;
                    case 1:
//                        ActivityManager.getInstance().startNextActivity(MyCollectionActivity.class);
                        break;
                    case 2:
                        ActivityManager.getInstance().startNextActivity(IncomeListActivity.class);
                        break;
                    case 3:
                        ActivityManager.getInstance().startNextActivity(QuestionActivity.class);
                        break;
                    case 4:
                        ActivityManager.getInstance().startNextActivity(FeedBackActivity.class);
                        break;
                    case 5:
                        ActivityManager.getInstance().startNextActivity(ContactUsActivity.class);
                        break;
                    case 6:
//                        ActivityManager.getInstance().startNextActivity(MyCollectionActivity.class);
                        break;
                    case 7:
                        ActivityManager.getInstance().startNextActivity(MyInviteActivity.class);
                        break;
                    case 8:
                        ActivityManager.getInstance().startNextActivity(NoticeActivity.class);
                        break;
                    default:
                        break;
                }
            }
        });
    }


    @OnClick({R.id.iv_avatar, R.id.tv_copy, R.id.tv_withdraw, R.id.l_my_income, R.id.l_my_order, R.id.l_my_fans})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_avatar:
                ActivityManager.getInstance().startNextActivity(SettingActivity.class);
                break;
            case R.id.tv_copy:
                ClipboardManagerUtil.copy(tvInviteCode.getText().toString());
                break;
            case R.id.tv_withdraw:

                break;
            case R.id.l_my_income:
                ActivityManager.getInstance().startNextActivity(MyIncomeActivity.class);
                break;
            case R.id.l_my_order:
                ActivityManager.getInstance().startNextActivity(MyOrderActivity.class);
                break;
            case R.id.l_my_fans:
                ActivityManager.getInstance().startNextActivity(MyFansActivity.class);
                break;
            default:
                break;
        }
    }

    @Override
    public void userInfo() {
        ToastUtils.showToast("获取用户信息");
    }
}
