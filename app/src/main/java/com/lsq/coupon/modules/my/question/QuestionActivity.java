package com.lsq.coupon.modules.my.question;

import android.os.Bundle;

import com.githang.statusbar.StatusBarCompat;
import com.lsq.coupon.R;
import com.lsq.coupon.base.BaseActivity;

/**
 * 常见问题
 */
public class QuestionActivity extends BaseActivity {

    @Override
    protected String getPagerTitle() {
        return getString(R.string.usually_question);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_question;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.white), true);
    }

    @Override
    protected void initData() {

    }
}
