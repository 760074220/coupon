package com.lsq.coupon.modules.my._presenter;

import android.content.Context;

import com.lsq.coupon.modules.my._model.MyM;
import com.lsq.coupon.modules.my._model.MyMImp;
import com.lsq.coupon.modules.my._view.CollectionStatusV;
import com.lsq.coupon.modules.my._view.IncomeListV;
import com.lsq.coupon.modules.my._view.MyFansV;
import com.lsq.coupon.modules.my._view.MyIncomeV;
import com.lsq.coupon.modules.my._view.MyOrderV;
import com.lsq.coupon.modules.my._view.SearchCollectionV;
import com.lsq.coupon.modules.my._view.WithDrawV;
import com.lsq.coupon.net.BaseInterfaceObject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by lsq on 2019/3/14.
 */

public class MyPImp implements MyP {

    private MyM myM;
    private CollectionStatusV collectionStatusV;
    private SearchCollectionV searchCollectionV;
    private MyFansV myFansV;
    private WithDrawV withDrawV;
    private MyIncomeV myIncomeV;
    private IncomeListV incomeListV;
    private MyOrderV myOrderV;

    public MyPImp(Context context, CollectionStatusV collectionStatusV, SearchCollectionV searchCollectionV, CompositeDisposable compositeDisposable) {
        this.myM = new MyMImp(context, compositeDisposable);
        this.collectionStatusV = collectionStatusV;
        this.searchCollectionV = searchCollectionV;
    }

    public MyPImp(Context context, MyFansV myFansV, CompositeDisposable compositeDisposable) {
        this.myM = new MyMImp(context, compositeDisposable);
        this.myFansV = myFansV;
    }

    public MyPImp(Context context, IncomeListV incomeListV, CompositeDisposable compositeDisposable) {
        this.myM = new MyMImp(context, compositeDisposable);
        this.incomeListV = incomeListV;
    }

    public MyPImp(Context context, MyOrderV myOrderV, CompositeDisposable compositeDisposable) {
        this.myM = new MyMImp(context, compositeDisposable);
        this.myOrderV = myOrderV;
    }

    public MyPImp(Context context, WithDrawV withDrawV, MyIncomeV myIncomeV, CompositeDisposable compositeDisposable) {
        this.myM = new MyMImp(context, compositeDisposable);
        this.withDrawV = withDrawV;
        this.myIncomeV = myIncomeV;
    }


    @Override
    public void searchMyCollection(int pageno, int pagesize) {
        myM.searchMyCollection(pageno, pagesize, new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {
                searchCollectionV.myCollectionInfo();
            }
        });
    }

    @Override
    public void sureCollection(String goodsId, String pic, int isTmall, String title, double price,
                               double orgPrice, double earnSum, int quanPrice) {
        myM.sureCollection(goodsId, pic, isTmall, title, price, orgPrice, earnSum, quanPrice, new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {
                collectionStatusV.changeCollectionStatus();
            }
        });
    }

    @Override
    public void cancelCollection(String goodsId) {
        myM.cancelCollection(goodsId, new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {
                collectionStatusV.changeCollectionStatus();
            }
        });
    }

    @Override
    public void getFansCount() {
        myM.getFansCount(new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {
                myFansV.fansCount();
            }
        });
    }

    @Override
    public void getFansList(int type, int listType, int pageno, int pagesize) {
        myM.getFansList(type, listType, pageno, pagesize, new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {
                myFansV.fansList();
            }
        });
    }

    @Override
    public void getMyIncome() {
        myM.getMyIncome(new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {
                myIncomeV.incomeInfo();
            }
        });
    }

    @Override
    public void wihtDraw(double amout) {
        myM.wihtDraw(amout, new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {
                withDrawV.withDrawResult();
            }
        });
    }

    @Override
    public void getWithDrawList(int pageno, int pagesize) {
        myM.getWithDrawList(pageno, pagesize, new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {

            }
        });
    }

    @Override
    public void getIncomeList() {
        myM.getIncomeList(new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {
                incomeListV.incomeListInfo();
            }
        });
    }

    @Override
    public void getOrderList(Integer status, String time, int pageno, int pagesize) {
        myM.getOrderList(status, time, pageno, pagesize, new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {
                myOrderV.orderList();
            }
        });
    }
}
