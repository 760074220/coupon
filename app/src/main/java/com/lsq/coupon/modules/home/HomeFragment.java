package com.lsq.coupon.modules.home;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.lsq.coupon.R;
import com.lsq.coupon.adapter.GridGoodAdapter;
import com.lsq.coupon.adapter.HomeBannerAdapter;
import com.lsq.coupon.adapter.HomeTabAdapter;
import com.lsq.coupon.adapter.LandscapeGoodAdapter;
import com.lsq.coupon.base.BaseFragment;
import com.lsq.coupon.data.HomeTabBean;
import com.lsq.coupon.utils.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.bingoogolapple.bgabanner.BGABanner;

/**
 * Created by lsq on 2018/4/25.
 */

public class HomeFragment extends BaseFragment {


    private static HomeFragment fragment;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.banner)
    BGABanner banner;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;
    @BindView(R.id.recycler_tab)
    RecyclerView recyclerTab;
    @BindView(R.id.iv_ad_one)
    ImageView ivAdOne;
    @BindView(R.id.iv_ad_two)
    ImageView ivAdTwo;
    @BindView(R.id.iv_ad_three)
    ImageView ivAdThree;
    @BindView(R.id.recycler_hot_sale)
    RecyclerView recyclerHotSale;
    @BindView(R.id.recycler_shipping)
    RecyclerView recyclerShipping;
    @BindView(R.id.recycler_select)
    RecyclerView recyclerSelect;

    //轮播图
    private HomeBannerAdapter homeBannerAdapter;
    private List<Integer> listBanner;

    //导航
    private HomeTabAdapter homeTabAdapter;
    private List<HomeTabBean> listTab;

    //包邮
    private GridGoodAdapter shippingAdapter;
    private List<String> listShipping;

    //热销
    private GridGoodAdapter hotSaleAdapter;
    private List<String> listHotSale;


    //精选宝贝
    private LandscapeGoodAdapter selectAdapter;
    private List<String> listSelect;


    public static HomeFragment getInstance() {
        if (fragment == null) {
            synchronized (HomeFragment.class) {
                if (fragment == null) {
                    fragment = new HomeFragment();
                }
            }
        }
        return fragment;
    }


    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_home;
    }


    @Override
    protected void initData() {
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        StatusBarUtil.changeStatusBarHeight(toolbar);
        setBanner();
        setTab();
        setHotSale();
        setShipping();
        setSelect();
    }

    private void setSelect() {
        listSelect = new ArrayList<>();
        listSelect.add("1");
        listSelect.add("1");
        listSelect.add("1");
        listSelect.add("1");
        selectAdapter = new LandscapeGoodAdapter(listSelect);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setSmoothScrollbarEnabled(true);
        linearLayoutManager.setAutoMeasureEnabled(true);
        recyclerSelect.setLayoutManager(linearLayoutManager);
        recyclerSelect.setAdapter(selectAdapter);
        recyclerSelect.setHasFixedSize(true);
        recyclerSelect.setNestedScrollingEnabled(false);
    }

    private void setShipping() {
        listShipping = new ArrayList<>();
        listShipping.add("1");
        listShipping.add("1");
        listShipping.add("1");
        listShipping.add("1");
        listShipping.add("1");
        shippingAdapter = new GridGoodAdapter(listShipping);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerShipping.setLayoutManager(linearLayoutManager);
        recyclerShipping.setAdapter(shippingAdapter);
    }

    private void setHotSale() {
        listHotSale = new ArrayList<>();
        listHotSale.add("1");
        listHotSale.add("1");
        listHotSale.add("1");
        listHotSale.add("1");
        listHotSale.add("1");
        hotSaleAdapter = new GridGoodAdapter(listHotSale);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerHotSale.setLayoutManager(linearLayoutManager);
        recyclerHotSale.setAdapter(hotSaleAdapter);
    }

    private void setTab() {
        listTab = new ArrayList<>();
        listTab.add(new HomeTabBean("9.9包邮", R.mipmap.ic_launcher));
        listTab.add(new HomeTabBean("9.9包邮", R.mipmap.ic_launcher));
        listTab.add(new HomeTabBean("9.9包邮", R.mipmap.ic_launcher));
        listTab.add(new HomeTabBean("9.9包邮", R.mipmap.ic_launcher));
        listTab.add(new HomeTabBean("9.9包邮", R.mipmap.ic_launcher));
        homeTabAdapter = new HomeTabAdapter(listTab);
        recyclerTab.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        recyclerTab.setAdapter(homeTabAdapter);
    }

    private void setBanner() {
        listBanner = new ArrayList<>();
        homeBannerAdapter = new HomeBannerAdapter(context);
        banner.setAdapter(homeBannerAdapter);
        listBanner.add(R.mipmap.pic_one);
        listBanner.add(R.mipmap.pic_two);
        listBanner.add(R.mipmap.pic_three);
        listBanner.add(R.mipmap.pic_four);
        banner.setData(R.layout.item_banner, listBanner, null);
    }


    @OnClick({R.id.iv_ad_one, R.id.iv_ad_two, R.id.iv_ad_three, R.id.tv_hot_sale, R.id.tv_shipping})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_ad_one:
                break;
            case R.id.iv_ad_two:
                break;
            case R.id.iv_ad_three:
                break;
            case R.id.tv_hot_sale:
                break;
            case R.id.tv_shipping:
                break;
        }
    }
}
