package com.lsq.coupon.modules.search;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lsq.coupon.R;
import com.lsq.coupon.adapter.LandscapeGoodAdapter;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.utils.DensityUtil;
import com.lsq.coupon.view.RadioButtonDrawableRight;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 搜索结果页
 */
public class SearchResultActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_complex)
    RadioButton tvComplex;
    @BindView(R.id.tv_coupon_amout)
    RadioButton tvCouponAmout;
    @BindView(R.id.tv_reward)
    RadioButton tvReward;
    @BindView(R.id.tv_featured)
    RadioButton tvFeatured;
    @BindView(R.id.tv_coupon_price)
    RadioButtonDrawableRight tvCouponPrice;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;


    private LandscapeGoodAdapter mAdapter;
    private List<String> list;

    private int typeObj = 1;
    private int typeOrder = 1;

    @Override
    protected String getPagerTitle() {
        return "分类搜索";
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_search_result;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        Drawable drawable = getResources().getDrawable(R.drawable.ic_search_sort_arrow);
        drawable.setBounds(DensityUtil.dp2px(this, 3), 0, DensityUtil.dp2px(this, 15), DensityUtil.dp2px(this, 13));
        tvCouponPrice.setCompoundDrawables(null, null, drawable, null);
        setAdapter();
    }

    private void setAdapter() {
        list = new ArrayList<>();
        list.add("1");
        list.add("1");
        list.add("1");
        list.add("1");
        mAdapter = new LandscapeGoodAdapter(list);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(mAdapter);
        /**
         * 上拉加载更多
         */
        mAdapter.disableLoadMoreIfNotFullPage(recycler);
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
//                if (listArrival.size() == pageNum * pageSize) {
//                    pageNum++;
//                    goodManagerPImp.getArrivalHistory( storeNo, edContact.getText().toString(), pageNum, pageSize);
//                } else {
//                    arrivalHistoryAdapter.loadMoreEnd(false);
//                }
            }
        }, recycler);

        /**
         * 下拉刷新
         */
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                if (swipe.isRefreshing()) {
//                    //关闭刷新动画
//                    swipe.setRefreshing(false);
//                }
            }
        });
    }

    @Override
    protected void initData() {

    }

    @OnClick({R.id.tv_complex, R.id.tv_coupon_amout, R.id.tv_reward, R.id.tv_featured, R.id.tv_coupon_price})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_complex:
                typeObj = 1;
                break;
            case R.id.tv_coupon_amout:
                typeObj = 2;
                break;
            case R.id.tv_reward:
                typeObj = 3;
                break;
            case R.id.tv_featured:
                typeObj = 4;
                break;
            case R.id.tv_coupon_price:
                if (typeObj == 5) {
                    typeOrder = typeOrder == 1 ? 2 : 1;
                    Drawable drawable = getResources().getDrawable(typeOrder == 1 ? R.drawable.ic_search_sort_arrow : R.drawable.ic_search_sort_arrow_p);
                    drawable.setBounds(DensityUtil.dp2px(this, 3), 0, DensityUtil.dp2px(this, 15), DensityUtil.dp2px(this, 13));
                    tvCouponPrice.setCompoundDrawables(null, null, drawable, null);
                } else typeObj = 5;
                break;
        }
    }
}
