package com.lsq.coupon.modules.start.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.lsq.coupon.base.UserManager;
import com.lsq.coupon.data.VersionBean;
import com.lsq.coupon.modules.start.model.CheckVersionM;
import com.lsq.coupon.modules.start.model.CheckVersionMImp;
import com.lsq.coupon.modules.start.view.CheckVersionV;
import com.lsq.coupon.net.BaseInterfaceObject;
import com.lsq.coupon.utils.VersionUtils;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by lsq on 2018/8/24.
 */

public class CheckVersionPImp implements CheckVersionP {

    private CheckVersionV checkVersion;
    private CheckVersionM checkVersionMImp;
    private Context context;

    public CheckVersionPImp(Context context, CheckVersionV checkVersion, CompositeDisposable compositeDisposable) {
        this.checkVersion = checkVersion;
        this.checkVersionMImp = new CheckVersionMImp(context, compositeDisposable);
        this.context = context;
    }

    @Override
    public void checkVersion() {
        checkVersionMImp.checkVersion(new BaseInterfaceObject() {
            @Override
            public void returnData(Object obj) {
                VersionBean versionBean = (VersionBean) obj;
                //服务器的版本
                String newVersion = versionBean.getVersionNum().toString();
                //现在储存的忽略版本号
                String ignoreVersion = UserManager.getInstance().getIgnoreVersion();
                //是否忽略版本
                String isIgnore = UserManager.getInstance().getIsIgnoreVersion();
                //现在版本
                String version = String.valueOf(VersionUtils.getVersionCode(context));
                //如果版本号现在版本号和服务器上面版本号相同 不更新
                if (version.equals(newVersion)) {
                    UserManager.getInstance().setIsIgnoreVersion("0");
                    UserManager.getInstance().setIgnoreVersion("");
                    return;
                }
                //如果不是强制更新
                if (versionBean.getForceUpdate() == 0) {
                    //如果忽略
                    if (!TextUtils.isEmpty(isIgnore) && isIgnore.equals("1")) {
                        //如果忽略版本号和当前版本一样
                        if (!TextUtils.isEmpty(ignoreVersion) && ignoreVersion.equals(newVersion)) {
                            return;
                        }
                    }
                }
                checkVersion.checkVersion(versionBean);
            }
        });
    }
}
