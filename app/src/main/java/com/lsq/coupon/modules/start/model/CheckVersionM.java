package com.lsq.coupon.modules.start.model;


import com.lsq.coupon.net.BaseInterfaceObject;

/**
 * Created by lsq on 2018/8/24.
 */

public interface CheckVersionM {
    void checkVersion(BaseInterfaceObject lis);
}
