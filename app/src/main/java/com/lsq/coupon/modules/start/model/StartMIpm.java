package com.lsq.coupon.modules.start.model;

import android.content.Context;

import com.lsq.coupon.net.BaseInterfaceObject;
import com.lsq.coupon.net.DisposableWrapper;
import com.lsq.coupon.net.LoadingDialog;
import com.lsq.coupon.net.RetrofitUtil;
import com.lsq.coupon.net.SimpleTransFormer;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by lsq on 2018/5/14.
 */

public class StartMIpm implements StartM {

    private CompositeDisposable compositeDisposable;
    private Context context;

    public StartMIpm(Context mContext, CompositeDisposable compositeDisposable) {
        this.compositeDisposable = compositeDisposable;
        this.context = mContext;
    }

    @Override
    public void login(String phone, String authCode, BaseInterfaceObject obj) {
        obj.returnData(null);
    }

    @Override
    public void getCode(String phone, int type, BaseInterfaceObject obj) {
        obj.returnData(null);
    }

    @Override
    public void logintOut(BaseInterfaceObject obj) {
        obj.returnData(null);
    }

    @Override
    public void modifyPassWord(String oldpwd, String newpwd, final BaseInterfaceObject lis) {
        compositeDisposable.add(
                // UserManager.getInstance().getUserInfoBean().getId()
                RetrofitUtil.getApiService().modifyPassWord(
                        0, oldpwd, newpwd)
                        .compose(new SimpleTransFormer<Object>(Object.class))
                        .subscribeWith(new DisposableWrapper<Object>(LoadingDialog.show(context)) {
                            @Override
                            public void onNext(Object obj) {
                                lis.returnData(obj);
                            }
                        }));
    }

    @Override
    public void register(String phone, String inviteCode, String authCode, String password, BaseInterfaceObject obj) {
        obj.returnData(null);
    }

    @Override
    public void forgetPassword(String phone, String authCode, String password, BaseInterfaceObject obj) {
        obj.returnData(null);
    }
}
