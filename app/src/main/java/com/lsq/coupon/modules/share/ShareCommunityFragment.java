package com.lsq.coupon.modules.share;

import android.os.Bundle;

import com.lsq.coupon.R;
import com.lsq.coupon.base.BaseFragment;

public class ShareCommunityFragment extends BaseFragment {

    private static ShareCommunityFragment fragment;

    public static ShareCommunityFragment getInstance() {
        if (fragment == null) {
            synchronized (ShareCommunityFragment.class) {
                if (fragment == null) {
                    fragment = new ShareCommunityFragment();
                }
            }
        }
        return fragment;
    }


    @Override
    protected void initData() {

    }

    @Override
    protected void init(Bundle savedInstanceState) {

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_share_community;
    }
}
