package com.lsq.coupon.modules.my.fans;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.lsq.coupon.R;
import com.lsq.coupon.adapter.MyFansAdapter;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.modules.my._presenter.MyP;
import com.lsq.coupon.modules.my._presenter.MyPImp;
import com.lsq.coupon.modules.my._view.MyFansV;
import com.lsq.coupon.utils.TabLayoutUnderlineUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 我的粉丝
 */
public class MyFansActivity extends BaseActivity implements MyFansV {


    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_all_num)
    TextView tvAllNum;
    @BindView(R.id.tv_vip_num)
    TextView tvVipNum;
    @BindView(R.id.tv_common_num)
    TextView tvCommonNum;
    @BindView(R.id.tab)
    TabLayout tab;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;
    private List<String> tabTitles;

    private MyP myP;
    private int pageno = 1;
    private int pagesize = 20;
    //1超级会员 2普通会员
    private int type = 1;
    //1全部 2一级会员 3二级会员
    private int listType = 1;

    private List<String> list;
    private MyFansAdapter mAdapter;

    @Override
    protected String getPagerTitle() {
        return getString(R.string.my_fans);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_my_fans;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        myP = new MyPImp(this, this, compositeDisposable);
        setTab();
        setAdapter();
    }

    @Override
    protected void initData() {

    }

    private void setTab() {
        tabTitles = new ArrayList<>();
        tabTitles.add(getString(R.string.all_fans));
        tabTitles.add(getString(R.string.directly_fans));
        tabTitles.add(getString(R.string.recommend_fans));
        for (int i = 0; i < tabTitles.size(); i++) {
            String title = tabTitles.get(i);
            View view = View.inflate(this, R.layout.tab_text_orange_black, null);
            TextView titleText = view.findViewById(R.id.title);
            titleText.setText(title);
//            titleText.setTextColor(R.drawable.selector_title_color_orange_black);
//            Drawable drawable = getResources().getDrawable(bottomTabIcons.getResourceId(i, 0));
//            drawable.setBounds(0, 0, DensityUtil.dp2px(this, 20), DensityUtil.dp2px(this, 20));
//            titleText.setCompoundDrawables(null, drawable, null, null);
            tab.addTab(tab.newTab().setCustomView(view));
        }

        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
//        TabLayoutUnderlineUtil.getInstance().setTabLine(tab,this,(int)getResources().getDimension(R.dimen.dp_14),(int)getResources().getDimension(R.dimen.dp_14));
        TabLayoutUnderlineUtil.getInstance().setTabLine(tab, this, (int) getResources().getDimension(R.dimen.dp_10), (int) getResources().getDimension(R.dimen.dp_10));
    }

    private void setAdapter() {
        list = new ArrayList<>();
        list.add("1");
        list.add("1");
        mAdapter = new MyFansAdapter(list);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(mAdapter);

        /**
         * 上拉加载更多
         */
//        mAdapter.disableLoadMoreIfNotFullPage(recycler);
//        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
//            @Override
//            public void onLoadMoreRequested() {
//                if (listArrival.size() == pageNum * pageSize) {
//                    pageNum++;
//                    goodManagerPImp.getArrivalHistory( storeNo, edContact.getText().toString(), pageNum, pageSize);
//                } else {
//                    arrivalHistoryAdapter.loadMoreEnd(false);
//                }
//            }
//        }, recycler);

        /**
         * 下拉刷新
         */
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                if (swipe.isRefreshing()) {
//                    //关闭刷新动画
//                    swipe.setRefreshing(false);
//                }
            }
        });
    }

    @Override
    public void fansCount() {

    }

    @Override
    public void fansList() {

    }
}
