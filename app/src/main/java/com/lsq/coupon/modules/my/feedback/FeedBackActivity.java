package com.lsq.coupon.modules.my.feedback;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.lsq.coupon.R;
import com.lsq.coupon.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 意见反馈
 */
public class FeedBackActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ed_contact)
    EditText edContact;
    @BindView(R.id.tv_number)
    TextView tvNumber;
    @BindView(R.id.iv_add)
    ImageView ivAdd;
    @BindView(R.id.tv_submit)
    TextView tvSubmit;

    @Override
    protected String getPagerTitle() {
        return getString(R.string.feedback);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_feed_back;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.white), true);
    }

    @Override
    protected void initData() {

    }


    @OnClick({R.id.iv_add, R.id.tv_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_add:
                break;
            case R.id.tv_submit:
                break;
        }
    }
}
