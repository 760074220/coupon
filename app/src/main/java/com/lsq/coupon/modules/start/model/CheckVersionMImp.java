package com.lsq.coupon.modules.start.model;

import android.content.Context;

import com.lsq.coupon.data.VersionBean;
import com.lsq.coupon.net.BaseInterfaceObject;
import com.lsq.coupon.net.DisposableWrapper;
import com.lsq.coupon.net.LoadingDialog;
import com.lsq.coupon.net.RetrofitUtil;
import com.lsq.coupon.net.SimpleTransFormer;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by lsq on 2018/8/24.
 */

public class CheckVersionMImp implements CheckVersionM {

    private Context context;
    private CompositeDisposable compositeDisposable;

    public CheckVersionMImp(Context context, CompositeDisposable compositeDisposable) {
        this.context = context;
        this.compositeDisposable = compositeDisposable;
    }

    @Override
    public void checkVersion(final BaseInterfaceObject lis) {
        compositeDisposable.add(RetrofitUtil.getApiService().checkVersion(1)
                .compose(new SimpleTransFormer<VersionBean>(VersionBean.class))
                .subscribeWith(new DisposableWrapper<VersionBean>(LoadingDialog.show(context)) {
                    @Override
                    public void onNext(VersionBean obj) {
                        if (obj != null) {
                            lis.returnData(obj);
                        }
                    }
                }));
    }
}
