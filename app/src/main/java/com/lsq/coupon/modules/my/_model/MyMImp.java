package com.lsq.coupon.modules.my._model;

import android.content.Context;

import com.lsq.coupon.net.BaseInterfaceObject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by lsq on 2019/3/14.
 */

public class MyMImp implements MyM {

    private Context context;
    private CompositeDisposable compositeDisposable;

    public MyMImp(Context context, CompositeDisposable compositeDisposable) {
        this.context = context;
        this.compositeDisposable = compositeDisposable;
    }

    @Override
    public void searchMyCollection(int pageno, int pagesize, BaseInterfaceObject lis) {
        lis.returnData(null);
    }

    @Override
    public void sureCollection(String goodsId, String pic, int isTmall, String title, double price, double orgPrice, double earnSum, int quanPrice, BaseInterfaceObject lis) {
        lis.returnData(null);
    }

    @Override
    public void cancelCollection(String goodsId, BaseInterfaceObject lis) {
        lis.returnData(null);
    }

    @Override
    public void getFansCount(BaseInterfaceObject lis) {
        lis.returnData(null);
    }

    @Override
    public void getFansList(int type, int listType, int pageno, int pagesize, BaseInterfaceObject lis) {
        lis.returnData(null);
    }

    @Override
    public void getMyIncome(BaseInterfaceObject lis) {
        lis.returnData(null);
    }

    @Override
    public void wihtDraw(double amout, BaseInterfaceObject lis) {
        lis.returnData(null);
    }

    @Override
    public void getWithDrawList(int pageno, int pagesize, BaseInterfaceObject lis) {
        lis.returnData(null);
    }

    @Override
    public void getIncomeList(BaseInterfaceObject lis) {
        lis.returnData(null);
    }

    @Override
    public void getOrderList(Integer status, String time, int pageno, int pagesize, BaseInterfaceObject lis) {
        lis.returnData(null);
    }
}
