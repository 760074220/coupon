package com.lsq.coupon.modules.my._model;

import com.lsq.coupon.net.BaseInterfaceObject;

/**
 * Created by lsq on 2019/3/14.
 */

public interface MyM {

    //查询我的收藏
    void searchMyCollection(int pageno, int pagesize, BaseInterfaceObject lis);

    //确认收藏
    void sureCollection(String goodsId, String pic, int isTmall, String title, double price,
                        double orgPrice, double earnSum, int quanPrice, BaseInterfaceObject lis);

    //取消收藏
    void cancelCollection(String goodsId, BaseInterfaceObject lis);

    //我的粉丝统计
    void getFansCount(BaseInterfaceObject lis);

    //我的粉丝列表
    void getFansList(int type, int listType, int pageno, int pagesize, BaseInterfaceObject lis);

    //获取我的收益信息
    void getMyIncome(BaseInterfaceObject lis);

    //提现
    void wihtDraw(double amout, BaseInterfaceObject lis);

    //提现列表
    void getWithDrawList(int pageno, int pagesize, BaseInterfaceObject lis);

    //获取用户排行榜
    void getIncomeList(BaseInterfaceObject lis);

    //获取我的订单
    void getOrderList(Integer status, String time, int pageno, int pagesize, BaseInterfaceObject lis);
}
