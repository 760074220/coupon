package com.lsq.coupon.modules.search;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lsq.coupon.R;
import com.lsq.coupon.base.BaseActivity;
import com.lsq.coupon.utils.ToastUtils;
import com.lsq.coupon.view.FlowLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * 订单搜索
 */
public class SearchActivity extends BaseActivity {


    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.flow_history)
    FlowLayout flowHistory;
    @BindView(R.id.flow_hot)
    FlowLayout flowHot;


    private List<String> listSearch;
    private List<String> listHot;

    @Override
    protected String getPagerTitle() {
        return "复制商品标题 先领券再下单";
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_search;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        initChildViews();
    }

    @Override
    protected void initData() {

    }


    @OnClick({R.id.tv_search, R.id.tv_cancel, R.id.tv_refresh})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_search:
                break;
            case R.id.tv_cancel:
                break;
            case R.id.tv_refresh:
                break;
        }
    }

    public void initChildViews() {
        listSearch = new ArrayList<>();
        listSearch.add("可以用淘口令、标题、链接来搜索");
        listSearch.add("羽绒服女");
        listSearch.add("羽绒服女");
        listSearch.add("羽绒服女");
        listSearch.add("可以用淘口令、标题、链接来搜索");
        addView(flowHistory, listSearch);

        listHot = new ArrayList<>();
        listHot.add("可以用淘口令、标题、链接来搜索");
        listHot.add("羽绒服女");
        listHot.add("羽绒服女");
        listHot.add("羽绒服女");
        listHot.add("可以用淘口令、标题、链接来搜索");
        addView(flowHot, listHot);
    }

    public void addView(FlowLayout flow, final List<String> list) {
        // TODO Auto-generated method stub
        ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        int marg = (int) getResources().getDimension(R.dimen.dp_5);
        int height = (int) getResources().getDimension(R.dimen.dp_25);
        lp.leftMargin = marg;
        lp.rightMargin = marg;
        lp.topMargin = marg;
        lp.bottomMargin = marg;
        for (int i = 0; i < list.size(); i++) {
            final int position = i;
            TextView view = new TextView(this);
            view.setHeight(height);
            view.setText(list.get(i));
            view.setTextColor(getResources().getColor(R.color.textColor33));
            view.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_gray_ededed_soild));
            view.setGravity(Gravity.CENTER);
            view.setPadding(marg, 0, marg, 0);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToastUtils.showToast(list.get(position));
                }
            });
            flow.addView(view, lp);
        }
    }

}
