package com.lsq.coupon.net;

import android.util.Log;

import com.lsq.coupon.utils.ToastUtils;

import java.net.UnknownHostException;

import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Created by lsq on 2017/6/19.
 */

public abstract class DisposableWrapper<T> extends DisposableSubscriber<T> {

    private static final String TAG = "DisposableWrapper";
    private LoadingDialog dialog;

    public DisposableWrapper() {

    }

    public DisposableWrapper(LoadingDialog dialog) {
        this.dialog = dialog;
    }

    @Override
    abstract public void onNext(T t);

    @Override
    public void onError(Throwable t) {
        if (t instanceof UnknownHostException) {
            ToastUtils.showToast("网络异常");
        }
        if (t instanceof ResponseError) {
            ResponseError responseError = (ResponseError) t;
            if (responseError.getErrorCode().equals("100003") ||
                    responseError.getErrorCode().equals("100004") ||
                    responseError.getErrorCode().equals("100005")) {
//                ToastUtil.showMessage(ActivityManager.getInstance().currentActivity(), "登录状态失效，请重新登录");
//                UserManager.logout(ActivityManager.getInstance().currentActivity());
            }
            if (Integer.parseInt(responseError.getErrorCode()) != 0) {
                if (responseError.getMessage().contains("请先登录")) {
//                    UserManager.logout(ActivityManager.getInstance().currentActivity());
                }
//                if (ActivityManager.getInstance().currentActivity() instanceof LoginActivity) {
//                    ToastUtil.showMessageGravity(ActivityManager.getInstance().currentActivity(), responseError.getMessage(), Gravity.CENTER);
//                } else
                ToastUtils.showToast(responseError.getMessage());
            }
        } else {
//            ToastUtil.showMessage(ActivityManager.getInstance().currentActivity(), t.getMessage());
        }
        Log.e(TAG, "onError: " + t.getMessage());
        onFinish();
    }


    @Override
    public void onComplete() {
        onFinish();
    }

    public void onFinish() {
        if (dialog != null) dialog.dismiss();
    }
}
