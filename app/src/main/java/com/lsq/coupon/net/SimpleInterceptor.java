package com.lsq.coupon.net;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * 拦截器
 * Created by lsq on 2017/6/12.
 */

public class SimpleInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {

//        if(){
//
//        }
        if (chain.request().url().toString().contains("/dyb-interface/scanQRCode")) {
            return chain.proceed(chain.request());
        }
        Request request = chain.request();

        RequestBody requestBody = request.body();
        Request.Builder newBuilder = request.newBuilder();
//        if (UserManager.isLogin() || !PreferencesUtil.getString(PreferenConstans.TOKENID).equals("")) {
//            if (null != UserManager.tokenId && !UserManager.tokenId.equals("")) {
//                KLog.e("lsq  -->tokenId", UserManager.tokenId);
//                newBuilder.addHeader("tokenId", UserManager.tokenId).build();
//            } else {
//                KLog.e("lsq  -->tokenId", PreferencesUtil.getString(PreferenConstans.TOKENID));
//                newBuilder.addHeader("tokenId", PreferencesUtil.getString(PreferenConstans.TOKENID)).build();
//            }
//        }

        Map<String, Object> map = new HashMap<>();
//        if (UserManager.isLogin()) {
//            map.put("token", UserManager.getUserToken(ActivityManager.getInstance().currentActivity()));
//        }
//        map.put("appKey", PropertiequestesUtil.getAppKey());
//        map.put("timestamp", System.currentTimeMillis() + "");
//        Random random = new Random();
//        map.put("noncestr", random.nextInt(999999 - 100000 + 1) + 100000 + "");
        if (requestBody instanceof FormBody) {
            //FormBody:指定Content-Type为application/x-www-form-urlencoded
            FormBody.Builder builder = new FormBody.Builder();
            FormBody formBody = (FormBody) requestBody;
            for (int i = 0; i < formBody.size(); i++) {
//                if (!TextUtils.isEmpty(formBody.value(i)))
//                    map.put(formBody.name(i), formBody.value(i));
                builder.addEncoded(formBody.encodedName(i), formBody.encodedValue(i));
            }

            //判断用户登录了就添加token
//            if (!TextUtils.isEmpty(UserManager.getInstance().getAccessToken())) {
//                formBody = builder.addEncoded("accessToken", UserManager.getInstance().getAccessToken()).build();
//            }

//            map.put("sign", SignUtil.getSign(map));
//            String json = GsonUtil.toJson(map);
//            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);
//            newBuilder.method(request.method(), body);

            newBuilder.post(formBody);

        } else if (requestBody instanceof MultipartBody) {
            MultipartBody body = (MultipartBody) requestBody;
            MultipartBody.Builder builder = new MultipartBody.Builder();
            List<MultipartBody.Part> parts = body.parts();
            //判断用户登录了就添加token
//            if (!TextUtils.isEmpty(UserManager.getInstance().getAccessToken())) {
//                KLog.e("lsq  -->accessToken", UserManager.getInstance().getAccessToken());
//                map.put("accessToken", UserManager.getInstance().getAccessToken());
//            }
            for (int i = 0; i < parts.size(); i++) {
                builder.addPart(parts.get(i));
            }
//            map.put("objectType", UploadFileType.getType());
            Set<Map.Entry<String, Object>> entries = map.entrySet();
            for (Map.Entry<String, Object> entry : entries) {
                builder.addFormDataPart(entry.getKey(), entry.getValue().toString());
            }
//            builder.addFormDataPart("sign", SignUtil.getSign(map));
            builder.setType(MultipartBody.FORM);
            MultipartBody build = builder.build();
            newBuilder.method(request.method(), build);
        }
        request = newBuilder.build();
//        Response response = chain.proceed(request);
//        if(!response.isSuccessful()){
//            KLog.e("lsq  -->","失败");
//        }
        return chain.proceed(request);
    }

}
