package com.lsq.coupon.net;

import java.util.List;

/**
 * Created by lsq on 2018/5/9.
 */
public interface BaseInterfaceList<T> {
    void returnData(List<T> list);
}
