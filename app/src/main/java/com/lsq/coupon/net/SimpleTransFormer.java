package com.lsq.coupon.net;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.lsq.coupon.base.ActivityManager;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

import io.reactivex.Flowable;
import io.reactivex.FlowableOperator;
import io.reactivex.FlowableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lsq on 2017/6/12.
 */

public class SimpleTransFormer<T> implements FlowableTransformer<SimpleResponse<T>, T> {

    Class clazz;
    T list;

    public SimpleTransFormer(Class clazz) {
        this.clazz = clazz;

    }

    public SimpleTransFormer(T list) {
        this.list = list;
    }

    @Override
    public Publisher<T> apply(Flowable<SimpleResponse<T>> upstream) {
        return upstream.subscribeOn(Schedulers.io())
                .flatMap(new Function<SimpleResponse<T>, Publisher<T>>() {
                    @Override
                    public Publisher<T> apply(@NonNull final SimpleResponse<T> tSimpleResponse) throws Exception {

                        T data = tSimpleResponse.data;
                        if (data == null) {
                            if (list != null) {
                                data = list;
                            } else { //创建类的新的实例
                                data = (T) clazz.newInstance();
                            }
                        }
                        return Flowable.just(data).lift(new FlowableOperator<T, T>() {
                            @Override
                            public Subscriber<? super T> apply(Subscriber<? super T> observer) throws Exception {
                                if (TextUtils.isEmpty(tSimpleResponse.code)) {
                                    tSimpleResponse.code = "01";
                                }
                                if (null != tSimpleResponse.code && tSimpleResponse.code.equals("3")) {
                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
//                                            ToastUtils.showToast("账号已在别处登录");
                                        }
                                    });
                                    observer.onError(new ResponseError(tSimpleResponse.code, tSimpleResponse.message));
                                    ActivityManager.getInstance().startLogin();
                                    return observer;
                                }

                                if (null != tSimpleResponse.code && tSimpleResponse.code.equals("443")) {
                                    //没提交tokenId
                                    observer.onError(new ResponseError(tSimpleResponse.code, "没提交tokenId"));
//                                    ActivityManager.getInstance().startLogin();
                                    return observer;
                                }

                                if (null != tSimpleResponse.message && (
                                        tSimpleResponse.message.equals("accessToken错误或不存在，请重新登录")
                                                || tSimpleResponse.code.equals("40014"))) {
                                    observer.onError(new ResponseError(tSimpleResponse.code, "没提交tokenId"));
                                    ActivityManager.getInstance().startLogin();
//                                    UploadTaskGpsService.stopServices();
                                    return observer;
                                }

                                if (null != tSimpleResponse.message && (tSimpleResponse.code.equals("40013"))) {
                                    observer.onError(new ResponseError(tSimpleResponse.code, tSimpleResponse.message));
                                    ActivityManager.getInstance().startLogin();
                                    return observer;
                                }

                                if (tSimpleResponse.success) {
                                    tSimpleResponse.code = "200";
                                }
//                                if (tSimpleResponse.code.equals("00")) {
                                //弹出提示
//                                final Activity activity = ActivityManager.getInstance().currentActivity();
//                                if (activity != null) {
//                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            if (!StringUtils.isEmpty(tSimpleResponse.message)) {
//                                                ToastUtils.showToast(tSimpleResponse.message);
//                                            }
//                                        }
//                                    });
//                                }
//                                }

//                                if (tSimpleResponse.data == null) {
//                                    observer.onError(new ResponseError("1", "没有获取到信息"));
//                                }

                                if (!(Integer.parseInt(tSimpleResponse.code) == 0)) {
                                    observer.onError(new ResponseError(tSimpleResponse.code, tSimpleResponse.message));
                                }
                                return observer;
                            }
                        });
                    }
                })
                .observeOn(AndroidSchedulers.mainThread());
    }
}
