package com.lsq.coupon.net;


import com.lsq.coupon.data.UserInfoBean;
import com.lsq.coupon.data.VersionBean;

import io.reactivex.Flowable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Administrator on 2017/9/25 0025.
 */

public interface Api {


    /**
     * 获得验证码
     */
    @FormUrlEncoded
    @POST(ApiConstants.GETCODE)
    Flowable<SimpleResponse<Object>> getCode(@Field("mobile") String phone,
                                             @Field("type") int type);

    /**
     * 注册
     */
    @FormUrlEncoded
    @POST(ApiConstants.REGISTER)
    Flowable<SimpleResponse<Object>> register(@Field("mobile") String phone,
                                              @Field("inviteCode") String inviteCode,
                                              @Field("verificationCode") String authCode,
                                              @Field("pwd") String password);

    /**
     * 登录  手机验证码登录
     */
    @FormUrlEncoded
    @POST(ApiConstants.LOGIN)
    Flowable<SimpleResponse<UserInfoBean>> loginCode(@Field("mobile") String phone,
                                                     @Field("type") int type,
                                                     @Field("verificationCode") String authCode);


    /**
     * 登录 账号密码登录
     */
    @FormUrlEncoded
    @POST(ApiConstants.LOGIN)
    Flowable<SimpleResponse<UserInfoBean>> loginAccount(@Field("mobile") String phone,
                                                        @Field("type") int type,
                                                        @Field("passWord") String passWord);

    /**
     * 退出登录
     */
    @FormUrlEncoded
    @POST(ApiConstants.LOGINOUT)
    Flowable<SimpleResponse<Object>> loginOut(@Field("token") String token);

    /**
     * 修改密码
     */
    @FormUrlEncoded
    @POST(ApiConstants.MODIFYPASSWORD)
    Flowable<SimpleResponse<Object>> modifyPassWord(@Field("id") Integer id,
                                                    @Field("newpwd") String newpwd,
                                                    @Field("oldpwd") String oldpwd);

    /**
     * 忘记密码
     */
    @FormUrlEncoded
    @POST(ApiConstants.MODIFYPASSWORD)
    Flowable<SimpleResponse<Object>> forgetPassword(@Field("mobile") String phone,
                                                    @Field("verificationCode") String authCode,
                                                    @Field("passWord") String password);


    /**
     * 检查版本号
     */
    @FormUrlEncoded
    @POST(ApiConstants.CHECKVERSION)
    Flowable<SimpleResponse<VersionBean>> checkVersion(@Field("type") int type);


    /**
     * 查询我的收藏
     */
    @FormUrlEncoded
    @POST(ApiConstants.SEARCHMYCOLLECTION)
    Flowable<SimpleResponse<Object>> searchMycollection(@Field("pageno") int pageno,
                                                        @Field("pagesize") int pagesize);

    /**
     * 确认我收藏
     */
    @FormUrlEncoded
    @POST(ApiConstants.SURECOLLECTION)
    Flowable<SimpleResponse<Object>> sureCollection(@Field("goodsId") String goodsId,
                                                    @Field("pic") String pic,
                                                    @Field("isTmall") int isTmall,
                                                    @Field("title") String title,
                                                    @Field("price") double price,
                                                    @Field("orgPrice") double orgPrice,
                                                    @Field("earnSum") double earnSum,
                                                    @Field("quanPrice") double quanPrice);

    /**
     * 取消收藏
     */
    @FormUrlEncoded
    @POST(ApiConstants.CANCELCOLLECTION)
    Flowable<SimpleResponse<Object>> cancelCollection(@Field("goodsId") String goodsId);


    /**
     * 粉丝统计
     */
    @FormUrlEncoded
    @POST(ApiConstants.GETFANSCOUNT)
    Flowable<SimpleResponse<Object>> getFansCount(@Field("empty") String empty);

    /**
     * 粉丝列表
     */
    @FormUrlEncoded
    @POST(ApiConstants.GETFANSLIST)
    Flowable<SimpleResponse<Object>> getFansList(@Field("type") int type,
                                                 @Field("listType") int listType,
                                                 @Field("pageno") int pageno,
                                                 @Field("pagesize") int pagesize);


    /**
     * 我的收益
     */
    @FormUrlEncoded
    @POST(ApiConstants.GETMYINCOME)
    Flowable<SimpleResponse<Object>> getMyIncome(@Field("empty") String empty);


    /**
     * 提现
     */
    @FormUrlEncoded
    @POST(ApiConstants.WITHDRAW)
    Flowable<SimpleResponse<Object>> withDraw(@Field("amount") double amount);

    /**
     * 提现列表
     */
    @FormUrlEncoded
    @POST(ApiConstants.WITHDRAWLIST)
    Flowable<SimpleResponse<Object>> withDrawList(@Field("pageno") int pageno,
                                                  @Field("pagesize") int pagesize);

    /**
     * 获取用户排行榜
     */
    @FormUrlEncoded
    @POST(ApiConstants.GETINCOMELIST)
    Flowable<SimpleResponse<Object>> getIncomeList(@Field("empty") String empty);

    /**
     * 获取订单列表
     */
    @FormUrlEncoded
    @POST(ApiConstants.GETORDERLIST)
    Flowable<SimpleResponse<Object>> getOrderList(@Field("status") Integer status,
                                                  @Field("time") String time,
                                                  @Field("pageno") int pageno,
                                                  @Field("pagesize") int pagesize);

}



