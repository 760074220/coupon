package com.lsq.coupon.net;

import java.io.Serializable;

/**
 * Created by lsq on 2017/6/12.
 */

public class SimpleResponse<T> implements Serializable{


    public T data;
//    public String returnCode;
    public String message;
    public String code;
//    public String success;
    public boolean success;
//    public String msg;
}
