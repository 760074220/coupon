package com.lsq.coupon.net;

/**
 * Created by Administrator on 2017/9/27 0027.
 */

public class SimpleNotBodyResponse<T> {


    /**
     * returnData : {"head":{"code":"01","msg":"成功"},"body":{}}
     */

    public ReturnDataBean<T> returnData;

    public class ReturnDataBean<T> {
        /**
         * head : {"code":"01","msg":"成功"}
         * body : {}
         */

        public HeadBean head;
        public T body;

        public class HeadBean {
            /**
             * code : 01
             * msg : 成功
             */

            public String code;
            public String msg;
        }

    }
}
