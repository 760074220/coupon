package com.lsq.coupon.net;

/**
 * Created by lsq on 2018/5/9.
 */
public interface BaseInterfaceObject<T> {
    void returnData(T obj);
}
