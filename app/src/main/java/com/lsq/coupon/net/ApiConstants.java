package com.lsq.coupon.net;

import android.text.TextUtils;

/**
 * Created by lsq on 2017/6/12.
 */

public class ApiConstants {

    //        public static final String BASE_URL = "http://adminapi.qdama.cn/";
    public static final String BASE_URL = "http://tesekeji.yicp.io:16048/";


    /**
     * http 超时时间(毫秒)
     */
    public static final long HTTP_TIME_OUT = 10000L;
    public static final long HTTP_TIME_OUT_CONNECT = 3000L;
    /**
     * HTTP最大连接数
     */
    public static final int HTTP_MAX_CONNECT_COUNT = 10;
    /**
     * HTTP保持KeepAlive的连接数
     */
    public static final long HTTP_KEEP_ALIVE_CONNECT_COUNT = 5;

    public static final String PATH = "appLoginCtl/applogin";

    public static final String IMAGE_PATH = "";
    //获得验证码
    public static final String GETCODE = BASE_URL + "registerOrLogin/sendMessage";
    //注册
    public static final String REGISTER = BASE_URL + "registerOrLogin/registerMobileInfo";
    //登录
    public static final String LOGIN = BASE_URL + "token/login";


    //退出登录
    public static final String LOGINOUT = BASE_URL + "api/v1/horseman/logout";
    //修改密码
    public static final String MODIFYPASSWORD = BASE_URL + "api/v1/systemuser/updatePassword";

    //检查更新
    public static final String CHECKVERSION = BASE_URL + "api/v1/app/appupdate/getversion";


    //查询我的收藏
    public static final String SEARCHMYCOLLECTION = BASE_URL + "collectionGoods/getByUserId";

    //用户收藏商品
    public static final String SURECOLLECTION = BASE_URL + "collectionGoods/updateCanceStatus";

    //用户取消收藏
    public static final String CANCELCOLLECTION = BASE_URL + "collectionGoods/save";

    //我的粉丝统计
    public static final String GETFANSCOUNT = BASE_URL + "mine/getTeamCount";

    //我的粉丝
    public static final String GETFANSLIST = BASE_URL + "mine/getTeamList";

    //我的收益
    public static final String GETMYINCOME = BASE_URL + "mine/myCommissionSum";

    //申请提现
    public static final String WITHDRAW = BASE_URL + "mine/withdrawalApplyFor";

    //申请提现
    public static final String WITHDRAWLIST = BASE_URL + "mine/withdrawalRecord";

    //获取用户排行榜
    public static final String GETINCOMELIST = BASE_URL + "mine/getCommissionList";

    //获取我的订单
    public static final String GETORDERLIST = BASE_URL + "mine/getOrderList";

//    public static final String GETORDERDETAILS = BASE_URL + "mine/getTeamOrderDetail";


    public static String getImageUrl(String imgUrl) {
        if (TextUtils.isEmpty(imgUrl)) {
            return "";
        }
        if (imgUrl.startsWith("http")) {
            return imgUrl;
        } else {
            return IMAGE_PATH + imgUrl;
        }
    }

    public static String getVideoUrl(String videoUrl) {
        if (TextUtils.isEmpty(videoUrl)) {
            return "";
        }
        if (videoUrl.startsWith("http")) {
            return videoUrl;
        } else {
            return BASE_URL + IMAGE_PATH + videoUrl;
        }
    }

//    public static String getFileUrl(String fileUrl) {
//        if (TextUtils.isEmpty(fileUrl)) {
//            return "";
//        }
//        if (fileUrl.startsWith("http")) {
//            return fileUrl;
//        } else return BASE_URL + IMAGE_PATH + fileUrl;
//    }
}
