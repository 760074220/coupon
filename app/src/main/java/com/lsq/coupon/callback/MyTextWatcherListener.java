package com.lsq.coupon.callback;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by lsq on 2018/12/19.
 * 空实现监听输入框
 */

public class MyTextWatcherListener implements TextWatcher{

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
