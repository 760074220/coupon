package com.lsq.coupon.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lsq.coupon.R;
import com.lsq.coupon.data.UserFragmentBean;
import com.lsq.coupon.utils.GlideUtil;

import java.util.List;

/**
 * Created by lsq on 2019/2/11.
 */

public class UserFragmentAdapter extends BaseQuickAdapter<UserFragmentBean, BaseViewHolder> {

    public UserFragmentAdapter(@Nullable List<UserFragmentBean> data) {
        super(R.layout.item_user_fragment, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, UserFragmentBean item) {
        GlideUtil.loadImage(mContext, item.getImg(), (ImageView) helper.getView(R.id.img));
        helper.setText(R.id.tv, item.getNeme());
    }
}
