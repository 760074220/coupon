package com.lsq.coupon.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lsq.coupon.R;

import java.util.List;

/**
 * Created by 76007 on 2019/2/19.
 */

public class MyOrderAdapter extends BaseQuickAdapter<String,BaseViewHolder>{
    public MyOrderAdapter( @Nullable List<String> data) {
        super(R.layout.item_order, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {

    }
}
