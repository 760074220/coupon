package com.lsq.coupon.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.lsq.coupon.utils.GlideUtil;

import cn.bingoogolapple.bgabanner.BGABanner;

/**
 * Created by lsq on 2019/2/28.
 */

public class HomeBannerAdapter implements BGABanner.Adapter<ImageView, Integer> {
    private Context context;

    public HomeBannerAdapter(Context context) {
        this.context = context;
    }

    @Override
    public void fillBannerItem(BGABanner banner, ImageView itemView, Integer model, int position) {
        GlideUtil.loadImage(context, model, itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }
}
