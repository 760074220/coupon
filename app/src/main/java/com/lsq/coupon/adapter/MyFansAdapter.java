package com.lsq.coupon.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lsq.coupon.R;

import java.util.List;

/**
 * Created by lsq on 2019/2/13.
 */

public class MyFansAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public MyFansAdapter(@Nullable List<String> data) {
        super(R.layout.item_fans, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {

    }
}
