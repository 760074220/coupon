package com.lsq.coupon.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lsq.coupon.R;
import com.lsq.coupon.data.HomeTabBean;
import com.lsq.coupon.utils.GlideUtil;

import java.util.List;

/**
 * Created by lsq on 2019/2/28.
 */

public class HomeTabAdapter extends BaseQuickAdapter<HomeTabBean, BaseViewHolder> {


    public HomeTabAdapter(@Nullable List<HomeTabBean> data) {
        super(R.layout.item_home_tab, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, HomeTabBean item) {
        GlideUtil.loadImage(mContext, item.getImg(), (ImageView) helper.getView(R.id.img));
        helper.setText(R.id.tv, item.getTitle());
    }
}
