package com.lsq.coupon.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lsq.coupon.R;

import java.util.List;

/**
 * Created by lsq on 2019/2/15.
 */

public class NoticeAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public NoticeAdapter(@Nullable List<String> data) {
        super(R.layout.item_notice, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {

    }
}
