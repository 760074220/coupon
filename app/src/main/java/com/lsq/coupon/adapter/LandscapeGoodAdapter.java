package com.lsq.coupon.adapter;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lsq.coupon.R;

import java.util.List;

/**
 * Created by lsq on 2019/2/12.
 */

public class LandscapeGoodAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public LandscapeGoodAdapter(@Nullable List<String> data) {
        super(R.layout.item_landscape_good, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {

        SpannableStringBuilder span = new SpannableStringBuilder("缩进" + "莲灿时尚女包红色真皮手拎包 2018年新款");
        span.setSpan(new ForegroundColorSpan(Color.TRANSPARENT), 0, 2,
                Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        helper.setText(R.id.tv_shop_name, span);
    }
}
