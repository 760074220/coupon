package com.lsq.coupon.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import java.util.UUID;

/**
 * 设备参数
 * Created by admin on 2016/12/5.
 */

public class DeviceInfo {

    private Activity mActivity;
    private DisplayMetrics metrics;
    private WindowManager windowManager;
    private TelephonyManager telephonyManager;
    private WifiManager wifiManager;
    private static final String TELEPHONY_SERVICE = Context.TELEPHONY_SERVICE;
    private static final String WIFI_SERVICE = Context.WIFI_SERVICE;

    public DeviceInfo(Activity act) {
        super();
        this.mActivity = act;
        metrics = new DisplayMetrics();
        telephonyManager = (TelephonyManager) mActivity.getSystemService(TELEPHONY_SERVICE);
        wifiManager = (WifiManager) mActivity.getApplicationContext().getSystemService(WIFI_SERVICE);
        windowManager = mActivity.getWindowManager();
        windowManager.getDefaultDisplay().getMetrics(metrics);
    }

    public static DeviceInfo instence;

    public static synchronized DeviceInfo getInstence(Activity activity) {
        if (instence == null) {
            instence = new DeviceInfo(activity);
        }
        return instence;
    }

    /**
     * 获取屏幕宽度
     *
     * @return int
     */
    public int getWidthPixels() {
        return metrics.widthPixels;
    }

    /**
     * 获取屏幕高度
     *
     * @return int
     */
    public int getHigthPixels() {
        return metrics.heightPixels;
    }

    /**
     * 获取屏幕密度
     *
     * @return int
     */
    public int getDensityDpi() {
        return metrics.densityDpi;
    }

    /**
     * 获取设备唯一编号 如果有些手机不能获取 imei(deviceId)，imei为null的话，传mac address,如果mac address为空，自动生成UID
     *
     * @return String
     */
    public String getDeviceId() {
        // String devid = telephonyManager.getDeviceId();
        // if (TextUtils.isEmpty(devid)) {
        WifiInfo info = wifiManager.getConnectionInfo();
        String macAddress = info.getMacAddress();
        if (TextUtils.isEmpty(macAddress)) {
            String uid = UUID.randomUUID().toString();
            uid = "uuid-" + uid;
            return uid;
        }
        return macAddress.replace(":", "");
//      /  }
        //   return devid;
    }

    /**
     * 手机SIM卡背面码<br>
     * ICCID， ICCID：Integrate circuit card identity 集成电路卡识别码（固化在手机SIM卡中） ICCID为IC卡的唯一识别号码，共有20位数字组成，其编码格式为：XXXXXX 0MFSS YYGXX XXXXX。 分别介绍如下： 前六位运营商代码：中国移动的为：898600；中国联通的为：898601。
     *
     * @return String
     */
    public String getIccid() {
        return telephonyManager.getSimSerialNumber();
    }

    /**
     * 手机SIM卡IMSI码<br>
     * IMEI imsi_国际移动用户识别码
     *
     * @return String
     */
    public String getImsi() {
        return telephonyManager.getSubscriberId();
    }

    /**
     * 手机类型： PHONE_TYPE_NONE 无信号 PHONE_TYPE_GSM GSM信号 PHONE_TYPE_CDMA CDMA信号
     *
     * @return int
     */
    public int getPhoneType() {
        return telephonyManager.getPhoneType();
    }

    /**
     * 检测网络是否为wifi
     *
     * @param mContext
     * @return
     */
    public boolean isWifi(Context mContext) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetInfo != null
                && activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {
            return true;
        }
        return false;
    }

}
