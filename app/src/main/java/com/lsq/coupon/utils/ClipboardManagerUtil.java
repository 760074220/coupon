package com.lsq.coupon.utils;

import android.content.ClipboardManager;
import android.content.Context;

import com.lsq.coupon.R;
import com.lsq.coupon.base.MyApplication;


/**
 * Created by lsq on 2018/9/1.
 * 粘贴板
 */

public class ClipboardManagerUtil {

    public static void copy(String contact) {
        ClipboardManager copy = (ClipboardManager) MyApplication.getInstance().getSystemService(Context.CLIPBOARD_SERVICE);
        copy.setText(contact);
        ToastUtils.showToast(contact + MyApplication.getInstance().getApplicationContext().getString(R.string.rider_toast_copy_succsess));
    }
}
