package com.lsq.coupon.utils;

import android.Manifest;
import android.app.Activity;
import android.support.v4.app.Fragment;


import com.lsq.coupon.base.Constants;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * @author lishuqi
 * @date 2018/2/12
 */

public class AccessRequestUtil {
    public static String[] permsPhoto = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static AccessRequestUtil getInstence() {
        return new AccessRequestUtil();
    }


    public static String[] permsAddress = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    /**
     * 检查拍照权限
     *
     * @param activity
     */
    @AfterPermissionGranted(Constants.REQUEST_PHOTO_PERMISSIONS)
    public boolean applyPhoto(Activity activity) {
        if (EasyPermissions.hasPermissions(activity, permsPhoto)) {
            return true;
        } else {
            EasyPermissions.requestPermissions(activity,
                    "请求相机权限", Constants.REQUEST_PHOTO_PERMISSIONS, permsPhoto);
        }
        return false;
    }

    /**
     * 检查定位权限
     *
     * @param activity
     */
    @AfterPermissionGranted(Constants.REQUEST_LOCATION_PERMISSIONS)
    public boolean applyAddress(Fragment activity) {
        if (EasyPermissions.hasPermissions(activity.getActivity(), permsAddress)) {
            return true;
        } else {
            EasyPermissions.requestPermissions(activity,
                    "请求定位权限", Constants.REQUEST_LOCATION_PERMISSIONS, permsAddress);
        }
        return false;
    }

    @AfterPermissionGranted(Constants.REQUEST_LOCATION_PERMISSIONS)
    public boolean applyAddress(Activity activity) {
        if (EasyPermissions.hasPermissions(activity, permsAddress)) {
            return true;
        } else {
            EasyPermissions.requestPermissions(activity,
                    "请求定位权限", Constants.REQUEST_LOCATION_PERMISSIONS, permsAddress);
        }
        return false;
    }

    @AfterPermissionGranted(Constants.REQUEST_SDCARD_PERMISSIONS)
    public static boolean checkSDCardPermission(Activity activity) {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(activity, perms)) {
            return true;
        } else {
            EasyPermissions.requestPermissions(activity,
                    "请求sd卡存储权限", Constants.REQUEST_SDCARD_PERMISSIONS, perms);
            return false;
        }
    }
}
