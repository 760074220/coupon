package com.lsq.coupon.utils;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Build;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.ViewGroup;

import com.lsq.coupon.base.ActivityManager;

/**
 * Created by Morphine on 2017/6/15.
 */

public class StatusBarUtil {

    public static void changeStatusBarHeight(Toolbar toolbar){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int statusBarHeight = StatusBarUtil.getStatusBarHigh(ActivityManager.getInstance().currentActivity());
            ViewGroup.LayoutParams layoutParams = toolbar.getLayoutParams();
            layoutParams.height += statusBarHeight;
            toolbar.setPadding(0, statusBarHeight, 0, 0);
            toolbar.setLayoutParams(layoutParams);
        }
    }

    public static int getStatusBarHigh(Activity activity) {
        int statusBarHeight = -1;
        //获取status_bar_height资源的ID
        int resourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            //根据资源ID获取响应的尺寸值
            statusBarHeight = activity.getResources().getDimensionPixelSize(resourceId);
        }
        if (statusBarHeight == -1) {
            Rect rectangle = new Rect();
            activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rectangle);
            statusBarHeight = rectangle.height();
        }
        if (statusBarHeight == -1) {
            DisplayMetrics dm = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
            //应用区域
            Rect outRect1 = new Rect();
            activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(outRect1);
            statusBarHeight = dm.heightPixels - outRect1.height();  //状态栏高度=屏幕高度-应用区域高度
        }
        return statusBarHeight;
    }

}