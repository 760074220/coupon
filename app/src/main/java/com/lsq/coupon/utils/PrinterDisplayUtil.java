package com.lsq.coupon.utils;

import android.app.Activity;
import android.view.Display;

/**
 * Created by lsq on 2019/1/22.
 */

public class PrinterDisplayUtil {
    public static void printerDisplay(Activity activity) {
        Display mDisplay = activity.getWindowManager().getDefaultDisplay();

        int W = mDisplay.getWidth();

        int H = mDisplay.getHeight();

        KLog.i("分辨率", "Width = " + W + "Height = " + H);
    }
}
