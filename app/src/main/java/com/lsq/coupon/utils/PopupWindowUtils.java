package com.lsq.coupon.utils;


import android.app.Activity;
import android.view.WindowManager;

/**
 * Created by lishuqi on 2017/5/2.
 */

public class PopupWindowUtils {

    public static PopupWindowUtils getInstence() {
        return new PopupWindowUtils();
    }



//    /**
//     * 提示框
//     *
//     * @param context
//     * @param message
//     */
//    public void suitablePopup(final Context context, String mTitle, String message, boolean hascancel, final Confirm confirmCall) {
//        if (message == null) {
//            message = "null";
//        }
//        View v = LayoutInflater.from(context).inflate(R.layout.popup_comm, null);
//        final PopupWindow popupwindow = new PopupWindow(v, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
//        v.getBackground().setAlpha(200);
//        TextView text = (TextView) v.findViewById(R.id.hint_text);
//        text.setText(message);
////        text.setTextColor(context.getResources().getColor(R.color._3b3c49));
//        text.setGravity(Gravity.CENTER);
//        TextView confirm = (TextView) v.findViewById(R.id.text_confirm);
//        TextView cancel = (TextView) v.findViewById(R.id.text_cancel);
//        TextView title = (TextView) v.findViewById(R.id.text_title);
//        confirm.requestFocus();
//        if (message.equals("尚未保存草稿")) {
//            confirm.setText("保存");
//            cancel.setText("不保存");
//        } else {
//            confirm.setText("确认");
//            cancel.setText("取消");
//        }
//        if (!hascancel) {
//            cancel.setVisibility(View.GONE);
////            title.setVisibility(View.GONE);
//        } else {
//            cancel.setVisibility(View.VISIBLE);
////            title.setVisibility(View.VISIBLE);
//        }
//        title.setText(mTitle);
//
//
//        //取消
//        cancel.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                popupwindow.dismiss();
//            }
//        });
//
//        //确认
//        confirm.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                popupwindow.dismiss();
//                if (confirmCall == null) {
//
//                } else {
//                    confirmCall.ConfirmBtnCallback(v);
//                }
//            }
//        });
//        popupwindow.setFocusable(true);
//        popupwindow.showAtLocation(((Activity) context).getWindow().getDecorView(), Gravity.CENTER, 0, 0);
//    }
//
//
    /**
     * 设置弹出popwindow时的页面背景为灰色
     * 显示   isShow = true
     */
    public static void setBackgruond(Activity activity, boolean isShow) {
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        if (isShow) {
            lp.alpha = 0.60f;
        } else {
            lp.alpha = 1f;
        }
        activity.getWindow().setAttributes(lp);
    }
//
//    /**
//     * 修改数量
//     */
//    private int number = 0;
//
//    public void modifyNumber(final Activity activity, final int count, final ModifyNumberListener listener) {
//        number = count;
//        View view = LayoutInflater.from(activity).inflate(R.layout.popup_modify_number, null);
//        setBackgruond(activity, true);
//        final PopupWindow modifyWindow = new PopupWindow(view.findViewById(R.id.pop_layout), LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        modifyWindow.setContentView(view);
//        TextView tvAdd = (TextView) view.findViewById(R.id.tv_add);
//        TextView tvLess = (TextView) view.findViewById(R.id.tv_less);
//        final EditText edNumber = (EditText) view.findViewById(R.id.ed_number);
//        TextView tvSure = (TextView) view.findViewById(R.id.tv_sure);
//        edNumber.setText(String.valueOf(number));
//        edNumber.setSelection(String.valueOf(number).length());
//        View.OnClickListener clickListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switch (v.getId()) {
//                    case R.id.tv_add:
//                        edNumber.setText(String.valueOf(number + 1));
//                        edNumber.setSelection(String.valueOf(number).length());
//                        break;
//                    case R.id.tv_less:
//                        if (number != 0) {
//                            edNumber.setText(String.valueOf(number - 1));
//                            edNumber.setSelection(String.valueOf(number).length());
//                        }
//                        break;
//                    case R.id.tv_sure:
//                        modifyWindow.dismiss();
//                        listener.getCount(number);
//                        break;
//                    default:
//                        break;
//                }
//            }
//        };
//        tvSure.setOnClickListener(clickListener);
//        tvAdd.setOnClickListener(clickListener);
//        tvLess.setOnClickListener(clickListener);
//
//        edNumber.addTextChangedListener(new MyTextCher() {
//            @Override
//            public void afterTextChanged(Editable s) {
//                number = Integer.valueOf(s.toString());
//                KLog.e("lsq  -->", s);
//            }
//        });
//
//
//        modifyWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
//            @Override
//            public void onDismiss() {
//                setBackgruond(activity, false);
//            }
//        });
//
//        modifyWindow.setFocusable(true);
//        modifyWindow.showAtLocation(activity.getWindow().getDecorView(), Gravity.CENTER, 0, 0);
//    }
//
//
//    /**
//     * 修改性别
//     */
//    private String sex;
//
//    public void modifySex(final Activity activity, final String string, final ModifySelectSexListener listener) {
//        sex = string;
//        View view = LayoutInflater.from(activity).inflate(R.layout.popup_select_sex, null);
//        setBackgruond(activity, true);
//        final PopupWindow modifyWindow = new PopupWindow(view.findViewById(R.id.pop_layout), LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        modifyWindow.setContentView(view);
//        LinearLayout lMan = (LinearLayout) view.findViewById(R.id.l_man);
//        ImageView ivMan = (ImageView) view.findViewById(R.id.iv_man);
//        LinearLayout lFemale = (LinearLayout) view.findViewById(R.id.l_female);
//        ImageView ivFemale = (ImageView) view.findViewById(R.id.iv_female);
//
//        if (sex.equals("1")) {
//            //表示男
//            ivMan.setImageResource(R.drawable.selected);
//        } else {
//            //表示女
//            ivFemale.setImageResource(R.drawable.selected);
//        }
//
//        View.OnClickListener clickListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switch (v.getId()) {
//                    case R.id.l_man:
//                        sex = "1";
//                        listener.selectSex(sex);
//                        modifyWindow.dismiss();
//                        break;
//                    case R.id.l_female:
//                        sex = "2";
//                        listener.selectSex(sex);
//                        modifyWindow.dismiss();
//                        break;
//                    default:
//                        break;
//                }
//            }
//        };
//        lMan.setOnClickListener(clickListener);
//        lFemale.setOnClickListener(clickListener);
//
//        modifyWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
//            @Override
//            public void onDismiss() {
//                setBackgruond(activity, false);
//            }
//        });
//
//        modifyWindow.setFocusable(true);
//        modifyWindow.showAtLocation(activity.getWindow().getDecorView(), Gravity.CENTER, 0, 0);
//    }
//
//    /**
//     * 选择登录角色
//     */
//    public void selectLogin(final Activity activity, final List<String> list, final SelectLoginListener listener) {
//        View view = LayoutInflater.from(activity).inflate(R.layout.popup_select_login, null);
//        PopupWindowUtils.setBackgruond(activity, true);
//        final PopupWindow selectLogin = new PopupWindow(view.findViewById(R.id.pop_layout), LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        selectLogin.setContentView(view);
//        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
//        TextView cancle = (TextView) view.findViewById(R.id.cancel);
//
//        SelectLoginAdapter mAdapter = new SelectLoginAdapter(activity, list);
//        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
//        recyclerView.setAdapter(mAdapter);
//        recyclerView.setNestedScrollingEnabled(false);
//        recyclerView.addItemDecoration(new DividerGridItemDecoration(activity));
//
//        mAdapter.setOnItemClickListener(new SelectLoginAdapter.OnRecyclerItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                listener.selectLogin(position);
//                selectLogin.dismiss();
//            }
//        });
//        cancle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                selectLogin.dismiss();
//            }
//        });
//        selectLogin.setOnDismissListener(new PopupWindow.OnDismissListener() {
//            @Override
//            public void onDismiss() {
//                PopupWindowUtils.setBackgruond(activity, false);
//            }
//        });
//
//        selectLogin.setFocusable(true);
//        selectLogin.showAtLocation((activity).getWindow().getDecorView(), Gravity.CENTER | Gravity.BOTTOM, 0, 0);
//    }
//
//
//    /**
//     * 展示二维码
//     *
//     * @param context
//     */
//    private PopupWindow popupwindow;
//
//    public PopupWindow suitableECode(final Context context, String mTitle, Bitmap bitmap, final Confirm confirmCall) {
//        View v = LayoutInflater.from(context).inflate(R.layout.popup_e_code, null);
//        popupwindow = new PopupWindow(v, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
//        v.getBackground().setAlpha(200);
//        TextView confirm = (TextView) v.findViewById(R.id.text_confirm);
//        TextView cancel = (TextView) v.findViewById(R.id.text_cancel);
//        TextView title = (TextView) v.findViewById(R.id.text_title);
//        ImageView imgE = (ImageView) v.findViewById(R.id.img_ecode);
//        ImageView imgCancle = (ImageView) v.findViewById(R.id.img_cancle);
//        confirm.requestFocus();
//        title.setText(mTitle);
//
//        imgE.setImageBitmap(bitmap);
//
//        //取消
//        cancel.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                popupwindow.dismiss();
//            }
//        });
//
//        //确认
//        confirm.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                popupwindow.dismiss();
//                if (confirmCall == null) {
//
//                } else {
//                    confirmCall.ConfirmBtnCallback(v);
//                }
//            }
//        });
//        imgCancle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                popupwindow.dismiss();
//            }
//        });
//        popupwindow.setFocusable(true);
//        popupwindow.showAtLocation(((Activity) context).getWindow().getDecorView(), Gravity.CENTER, 0, 0);
//        return popupwindow;
//    }


}
