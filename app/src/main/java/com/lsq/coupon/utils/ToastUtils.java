package com.lsq.coupon.utils;

import android.widget.Toast;

import com.lsq.coupon.base.MyApplication;


/**
 * Created by lishuqi on 2017/8/5.
 */

public class ToastUtils {
    private static Toast toast;

    public static void showToast(String msg) {
        if (toast == null) {
            toast = Toast.makeText(MyApplication.getInstance().getApplicationContext(), msg, Toast.LENGTH_SHORT);
        } else {
            toast.cancel();//关闭吐司显示
            toast = Toast.makeText(MyApplication.getInstance().getApplicationContext(), msg, Toast.LENGTH_SHORT);
        }

        toast.show();//重新显示吐司
    }
}
