package com.lsq.coupon.utils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.lsq.coupon.R;

/**
 * Created by lsq on 2019/2/12.
 */

public class RecycleViewEmptyViewUtils {

    public View getView(Context context, RecyclerView recyclerView,String title) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_text_empty, recyclerView, false);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        tvMsg.setText(title);
        return view;
    }
}
