package com.lsq.coupon.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.view.View;
import android.widget.LinearLayout;

import java.lang.reflect.Field;

/**
 * Created by lsq on 2018/11/19.
 */

public class TabLayoutUnderlineUtil {
    private static TabLayoutUnderlineUtil instance = null;

    private TabLayoutUnderlineUtil() {

    }

    public static TabLayoutUnderlineUtil getInstance() {
        if (instance == null) {
            instance = new TabLayoutUnderlineUtil();
        }
        return instance;
    }


    /**
     * 设置tab下划线的长度
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void setTabLine(TabLayout tab, Context context, int left, int right) {
        try {
            Class<?> tablayout = tab.getClass();
            Field tabStrip = tablayout.getDeclaredField("mTabStrip");
            tabStrip.setAccessible(true);
            LinearLayout ll_tab = (LinearLayout) tabStrip.get(tab);
            for (int i = 0; i < ll_tab.getChildCount(); i++) {
                View child = ll_tab.getChildAt(i);
                child.setPadding(0, 0, 0, 0);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1);
                //修改两个tab的间距
                params.setMarginStart(DensityUtil.dp2px(context, left));
                params.setMarginEnd(DensityUtil.dp2px(context, right));
                child.setLayoutParams(params);
                child.invalidate();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
