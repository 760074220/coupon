package com.lsq.coupon.utils.transformer;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by lsq on 2019/2/15.
 */

public abstract class BasePageTransformer implements ViewPager.PageTransformer
{
    protected ViewPager.PageTransformer mPageTransformer = new NonPageTransformer();
    public static final float DEFAULT_CENTER = 0.5f;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void transformPage(View view, float position)
    {
        if (mPageTransformer != null)
        {
            mPageTransformer.transformPage(view, position);
        }

        pageTransform(view, position);
    }

    protected abstract void pageTransform(View view, float position);


}