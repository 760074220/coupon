package com.lsq.coupon.utils.picture;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.lsq.coupon.R;
import com.lsq.coupon.utils.luban.Luban;
import com.tangxiaolv.telegramgallery.GalleryActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import com.lsq.coupon.utils.DateUtils;
import com.lsq.coupon.utils.KLog;
import com.lsq.coupon.utils.PopupWindowUtils;


/**
 * Created by lishuqi on 2017/11/20.
 */

public class PictureDialog {

    public static String path = Environment.getExternalStorageDirectory()
            .getAbsolutePath() + "/AAA/";
    public static String pathAndroid = "AAA";

    /**
     * 检查权限
     */
    public static boolean setPhoto(Context context) {
        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    /**
     * 获取储存位置
     */
    public static String getPath() {
        File tmpFile = new File(path);
        if (!tmpFile.exists()) {
            tmpFile.mkdir();
        }
        StringBuffer s = new StringBuffer(path);
        s.append(DateUtils.getCurrentTimeTamp());
        s.append(".jpg");
        return s.toString();
    }

    /**
     * 获得自己系统的目录
     */
    public static String getPath(Context context) {
        File cacheDir = context.getExternalCacheDir();
        if (cacheDir != null) {
            File result = new File(cacheDir, pathAndroid);
            if (!result.mkdirs() && (!result.exists() || !result.isDirectory())) {
                // File wasn't able to create a directory, or the result exists but not a directory
                return null;
            }
            return result + "/" + DateUtils.getCurrentTimeTamp() + ".jpg";
        }
        return null;
    }


    /**
     * 选择照片
     * 从相册选择还是直接拍照
     * activit  进来的
     */
    public static void selectPicture(final Activity activity, final String filePath) {
        View view = LayoutInflater.from(activity).inflate(R.layout.popup_select_photo_center, null);
        PopupWindowUtils.setBackgruond(activity, true);
        final PopupWindow selectPicWindow = new PopupWindow(view.findViewById(R.id.pop_layout), LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        selectPicWindow.setContentView(view);

        TextView btn_take_photo = (TextView) view.findViewById(R.id.btn_take_photo);
        TextView btn_pick_photo = (TextView) view.findViewById(R.id.btn_pick_photo);
        TextView cancel = (TextView) view.findViewById(R.id.cancel);

        btn_take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri;
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                    uri = Uri.fromFile(new File(filePath));
                } else {
                    uri = FileProvider.getUriForFile(activity, activity.getPackageName() + ".provider", new File(filePath));
                }
                activity.startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE).putExtra(MediaStore.EXTRA_OUTPUT, uri), 103);
                selectPicWindow.dismiss();
            }
        });

        btn_pick_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPicWindow.dismiss();
                selectPhoto(activity, 101);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPicWindow.dismiss();
            }
        });

        selectPicWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                PopupWindowUtils.setBackgruond(activity, false);
            }
        });

        selectPicWindow.setFocusable(true);
        selectPicWindow.showAtLocation((activity).getWindow().getDecorView(), Gravity.CENTER | Gravity.BOTTOM, 0, 0);
    }

    /**
     * 选择照片
     * 从相册选择还是直接拍照
     * fragment  进来的
     */
    public static void selectPicture(final Fragment fragment, final String filePath) {
        View view = LayoutInflater.from(fragment.getContext()).inflate(R.layout.popup_select_photo_center, null);
        PopupWindowUtils.setBackgruond(fragment.getActivity(), true);
        final PopupWindow selectPicWindow = new PopupWindow(view.findViewById(R.id.pop_layout), LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        selectPicWindow.setContentView(view);

        TextView btn_take_photo = (TextView) view.findViewById(R.id.btn_take_photo);
        TextView btn_pick_photo = (TextView) view.findViewById(R.id.btn_pick_photo);
        TextView cancel = (TextView) view.findViewById(R.id.cancel);

        btn_take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                fragment.startActivityForResult(intent, 103);
                Uri uri;
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                    uri = Uri.fromFile(new File(filePath));
                } else {
                    uri = FileProvider.getUriForFile(fragment.getContext(), fragment.getContext().getPackageName() + ".provider", new File(filePath));
                }
                fragment.startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE).putExtra(MediaStore.EXTRA_OUTPUT, uri), 103);
                selectPicWindow.dismiss();
            }
        });

        btn_pick_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPicWindow.dismiss();
                selectPhoto(fragment , 101);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPicWindow.dismiss();
            }
        });

        selectPicWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                PopupWindowUtils.setBackgruond(fragment.getActivity(), false);
            }
        });

        selectPicWindow.setFocusable(true);
        selectPicWindow.showAtLocation((fragment.getActivity()).getWindow().getDecorView(), Gravity.CENTER | Gravity.BOTTOM, 0, 0);
    }

    /**
     * 从相册选择图片
     */
    public static void selectPhoto(Activity activity, int requestCode) {
        //第三方开源项目
//        地址：http://www.jcodecraeer.com/a/opensource/2016/0923/6635.html
        GalleryActivity.openActivity(activity, false,//true 单选，false 多选
                1,//图片可选数量限制，当singlePhoto=false时生效
                requestCode);//请求码
    }

    public static void selectPhoto(Fragment fragment, int requestCode) {
        //第三方开源项目
        //地址：http://www.jcodecraeer.com/a/opensource/2016/0923/6635.html
        MyGallerActivity.openActivity(fragment, false,//true 单选，false 多选
                1,//图片可选数量限制，当singlePhoto=false时生效
                requestCode);//请求码
    }

    /**
     * 图片压缩方法
     */
    public static File compressWithRx(Context context, File file) {
//        KLog.i("lsq    没压缩-->", file.length() / 1024 + "k");
//        KLog.i("lsq    没压缩-->", computeSize(file)[0] + "*" + computeSize(file)[1]);
//        File f = null;
//        try {
//            f = new Compressor(context).compressToFile(f);
//            KLog.i("lsq    压缩-->", f.length() / 1024 + "k");
//            KLog.i("lsq    压缩-->", computeSize(f)[0] + "*" + computeSize(f)[1]);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        KLog.e("lsq    没压缩-->", file.length() / 1024 + "k");
        KLog.e("lsq    没压缩-->", computeSize(file)[0] + "*" + computeSize(file)[1]);
        File f = null;
        try {
            f = Luban.with(context).load(file).get();
            KLog.e("lsq    压缩-->", f.length() / 1024 + "k");
            KLog.e("lsq    压缩-->", computeSize(f)[0] + "*" + computeSize(f)[1]);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return f;
    }

    /**
     * 检查图片的像素
     */
    public static int[] computeSize(File srcImg) {
        int[] size = new int[2];

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inSampleSize = 1;

        BitmapFactory.decodeFile(srcImg.getAbsolutePath(), options);
        size[0] = options.outWidth;
        size[1] = options.outHeight;

        return size;
    }

    /**
     * 将inoutsreame写入到ByteArrayOutputStream  检测文件的大小
     */
    public static byte[] WriteFromStream(Context context, File file) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            InputStream is = context.getContentResolver().openInputStream(Uri.fromFile(file));
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = is.read(buffer)) != -1) {
                baos.write(buffer, 0, len);
            }
            is.close();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return baos.toByteArray();
    }
}
