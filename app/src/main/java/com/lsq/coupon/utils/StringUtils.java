package com.lsq.coupon.utils;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

	/**
	 * 判断给定字符串是否空白串。 空白串是指由空格、制表符、回车符、换行符组成的字符串 若输入字符串为null或空字符串，返回true
	 *
	 * @param input
	 * @return boolean
	 */
	public static boolean isEmpty(String input) {
		if (input == null || "".equals(input))
			return true;

		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			if (c != ' ' && c != '\t' && c != '\r' && c != '\n') {
				return false;
			}
		}
		return true;
	}
	public static boolean isEmpty(Object str) {
		if (str == null || str.equals("") || "null".equals(str)
				|| "-1".equals(str + "")) {
			return true;
		}
		return false;
	}

	/**
	 * 去掉所有空格
	 * 
	 * @param str
	 * @return
	 */
	public static String replaceBlank(String str) {
		String dest = "";
		if (str != null) {
			Pattern p = Pattern.compile("\\s*|\t|\r|\n");
			Matcher m = p.matcher(str);
			dest = m.replaceAll("");
		}
		return dest;
	}

	public static String getUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}

	/**
	 * 返回一个高亮spannable
	 * 
	 * @param content
	 *            文本内容
	 * @param color
	 *            高亮颜色
	 * @param start
	 *            起始位置
	 * @param end
	 *            结束位置
	 * @return 高亮spannable
	 */
	public static CharSequence getHighLightText(String content, int color,
			int start, int end) {
		if (TextUtils.isEmpty(content)) {
			return "";
		}
		start = start >= 0 ? start : 0;
		end = end <= content.length() ? end : content.length();
		SpannableString spannable = new SpannableString(content);
		CharacterStyle span = new ForegroundColorSpan(color);
		spannable.setSpan(span, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		return spannable;
	}

	/**
	 * 是否为数字
	 *
	 */
	public static boolean isNumber(String digitString) {
		if (!TextUtils.isEmpty(digitString)) {
			String regex = "[0-9]*";
			return isMatch(regex, digitString);
		}
		return false;
	}

	/**
	 * 是否为数字,并且判断位数
	 * 
	 * @param digitString
	 *            校验的字符串
	 * @param len
	 *            校验的长度
	 * @return 返回ReturnInfo类数据
	 */
	public static boolean isNumber(String digitString, int len) {
		if (!TextUtils.isEmpty(digitString)) {
			String regex = "[0-9]{" + len + "}";
			return isMatch(regex, digitString);
		}
		return false;
	}

	/**
	 * 字符串正则校验
	 *
	 */
	public static boolean isMatch(String regex, String string) {

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(string);
		return matcher.matches();
	}

	/**
	 * 格式化11位手机号
	 *
	 */
	public static String getFormatPhone(String phone) {
		if (TextUtils.isEmpty(phone)) {
			return "";
		}
		if (phone.length() == 11) {
			return phone.substring(0, 3) + " " + phone.substring(3, 7) + " "
					+ phone.substring(7, 11);
		}
		return phone;
	}

	/**
	 * 确定提示框类型（有左右两个按纽的）
	 * 
	 * @version 1.0
	 */
	public interface TextShowType {
		/** 金额 ￥10,700.00 **/
		int TYPE_MONEY = 0;
	}

	/**
	 * 设置字符串显示方式
	 * 
	 * @param text
	 * @return
	 * @version 1.0
	 */
	public static String setFormatText(int type, String text) {
		String temp = text;
		switch (type) {
		case TextShowType.TYPE_MONEY:
			BigDecimal bdecimal = new BigDecimal(text);
			bdecimal = bdecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
			NumberFormat nf = DecimalFormat.getCurrencyInstance();
			temp = nf.format(bdecimal);
			break;

		}
		return temp;
	}

	public static String joinBySeparator(List<String> list) {
		String separators = "";
		for (String str : list) {
			separators += str + ",";
		}
		return separators.substring(0, separators.length() - 1);
	}

	public static String formatPrice(String priceStr) {
		try {
			double price =Double.parseDouble(priceStr);
			return formatPrice(price)+"元";
		} catch (Exception e) {
			return "0.00元";
		}
	}
	
	
	public static String formatPriceAnd(double price) {
		return formatPrice(price)+"元";
	}
	
	public static String formatPrice(double price) {
		if(price==0){
			return "0.00";
		}
		DecimalFormat dformat = new DecimalFormat("#0.00");
		return dformat.format(price);
	}
	
	
	/**
	 * 为字符隐藏打符号
	 * 截取并替换从startIndex到end  String phone=replaceString(myuser.getUserInfo().getMobile(), "****",3, 7); 
	 * @param source 原字符串
	 * @param rstr   替换的
	 * @param startIndex
	 * @param end
	 * @version 1.0
	 */
	public static String replaceString(String source,String rstr,int startIndex,int end){
		if (TextUtils.isEmpty(source))
			return "";
		if (startIndex > source.length() - 1) {
			// log.e("截取手机号码越界了");
			return "";
		}
		if(source.length()==2)
		return source.substring(0, 1)+rstr;	
		String preStr = source.substring(0, startIndex);
		String subStr = source.substring(end);
		return preStr + rstr + subStr;
	}
	/**
	 * 为字符隐藏打'*'
	 * 截取并替换从startIndex到end  String phone=replaceString(myuser.getUserInfo().getMobile(), "****",3, 7); 
	 * @param source 原字符串
	 * @param rstr   替换的
	 * @param startIndex
	 * @param end
	 * @version 1.0
	 */
	public static String replaceString(String source, int startIndex, int end) {
		if (TextUtils.isEmpty(source))
			return "";
		if (startIndex > source.length() - 1) {
			// log.e("截取手机号码越界了");
			return source;
		}
		if(source.length()==2)
		return source.substring(0, 1)+"*";	
		if(end<=startIndex)
		return source;	
		String rstr="";
		for(int i=end-startIndex;i>0;i--){
			rstr=rstr+"*";
		}
		String preStr = source.substring(0, startIndex);
		String subStr = source.substring(end);
		return preStr + rstr + subStr;
	}
	
	/**
	 * 是否正常取值范围
	 * @version 1.0
	 */
	public static boolean checkRange(String value) {
		if(TextUtils.isEmpty(value))
			return false;
		float temp=Float.parseFloat(value);
		if(temp<=0||temp>999999999.99){
			return false;
		}else
		return true;
	}
	
	/**
	 * 是否正常取值范围(注意整数8位已是科学计数了)
	 * @version 1.0
	 */
	public static boolean checkRange2(String value) {
		if(TextUtils.isEmpty(value))
			return false;
		float temp=Float.parseFloat(value);
		if(temp<=0||temp>9999999.49){//>=9999999.5已是科学计数了
			return false;
		}else
		return true;
	}
	/*
    固定电话
     */
	public static boolean isTelephone(String telephone){
		Pattern p = Pattern
				.compile("^(((010)|(020)|(021)|(022)|(023)|(024)|(025)|(026)|(027)|(028)|(029)|(0[3-9][1-9]{2}))\\d{7,8})$");
		Matcher m = p.matcher(telephone);

		return m.matches();
	}
	/**
	 * 手机号码校验
	 * @param mobiles
	 * @return
	 */
//	public static boolean isMobileNumber(String mobiles) {
//		return Pattern.compile("^((13[0-9])|(14[0-9])|(15[0-9])|(18[0-9])|(17[0-9])|(19[0-9])|(16[0-9]))\\d{8}").matcher(mobiles).matches();
//	}
	/**
	 * 手机号码校验（只验证11位）
	 * @param mobiles
	 * @return
	 */
	public static boolean isMobileNumber(String mobiles) {
		return Pattern.compile("^(1)\\d{10}").matcher(mobiles).matches();
	}

	/*
        检测是否包含中文
         */
	public static boolean isContainChinese(String str) {
		String regEx = "[\\u4E00-\\u9FA5]+";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(str);
		if (m.find()) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 文件大小转换
	 *
	 * @param size
	 * @return
	 */
	public static String fileSize(String size) {

		if (size.length() < 4) {
			return size + "B";
		} else if (size.length() >= 4 && size.length() < 8) {
			float i = Float.valueOf(size) / 1000;
			BigDecimal b = new BigDecimal(i);
			return String.valueOf(b.setScale(2, BigDecimal.ROUND_HALF_UP))
					+ "KB";
		} else if (size.length() >= 8) {
			float i = Float.valueOf(size) / 1000000;
			BigDecimal b = new BigDecimal(i);
			return String.valueOf(b.setScale(2, BigDecimal.ROUND_HALF_UP))
					+ "MB";
		}

		return "0";
	}
	/**
	 * 判断特殊字符
	 * 只允许字母，汉字，数字，_ -
	 */
	public static boolean isFilter(String str) {

		if (str.replaceAll("[\u4e00-\u9fa5\\w]*[a-z]*[A-Z]*\\d*-*_*\\s*", "")
				.length() == 0)
			return true;
		else
			return false;
	}
	/**
	 * 判断是不是一个合法的电子邮件地址
	 *
	 * @param email
	 * @return
	 */
	public static boolean isEmail(String email) {

		String str = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		Pattern p = Pattern.compile(str);
		Matcher m = p.matcher(email);

		return m.matches();
	}
	private final static ThreadLocal<SimpleDateFormat> dateFormater = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}
	};

	private final static ThreadLocal<SimpleDateFormat> dateFormater2 = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd");
		}
	};
	/**
	 * 将字符串转位日期类型
	 *
	 * @param sdate
	 * @return
	 */
	public static Date toDate(String sdate) {
		try {
			return dateFormater.get().parse(sdate);
		} catch (ParseException e) {
			return null;
		}
	}
	/**
	 * float类型如果小数点后为零则显示整数否则保留
	 */
	public static String trimFloat(float value) {
		if (Math.round(value) - value == 0) {
			return String.valueOf((long) value);
		}
		return String.valueOf(value);
	}
	/**
	 * 以友好的方式显示时间
	 *
	 * @param sdate
	 * @return
	 */
	public static String friendly_time(String sdate) {
		Date time = toDate(sdate);
		if (time == null) {
			return "Unknown";
		}
		String ftime = "";
		Calendar cal = Calendar.getInstance();

		// 判断是否是同一天
		String curDate = dateFormater2.get().format(cal.getTime());
		String paramDate = dateFormater2.get().format(time);
		if (curDate.equals(paramDate)) {
			int hour = (int) ((cal.getTimeInMillis() - time.getTime()) / 3600000);
			if (hour == 0)
				ftime = Math.max(
						(cal.getTimeInMillis() - time.getTime()) / 60000, 1)
						+ "分钟前";
			else
				ftime = hour + "小时前";
			return ftime;
		}

		long lt = time.getTime() / 86400000;
		long ct = cal.getTimeInMillis() / 86400000;
		int days = (int) (ct - lt);
		if (days == 0) {
			int hour = (int) ((cal.getTimeInMillis() - time.getTime()) / 3600000);
			if (hour == 0)
				ftime = Math.max(
						(cal.getTimeInMillis() - time.getTime()) / 60000, 1)
						+ "分钟前";
			else
				ftime = hour + "小时前";
		} else if (days == 1) {
			ftime = "昨天";
		} else if (days == 2) {
			ftime = "前天";
		} else if (days > 2 && days <= 10) {
			ftime = days + "天前";
		} else if (days > 10) {
			ftime = dateFormater2.get().format(time);
		}
		return ftime;
	}
	public static SpannableStringBuilder setTextColor(String string, String tag, int colorId){
		if(TextUtils.isEmpty(string) || TextUtils.isEmpty(tag)){
			new IllegalArgumentException("string or tag not can null");
		}
		SpannableStringBuilder style = new SpannableStringBuilder(string);
		int fstart = string.indexOf(tag);
		int fend = fstart + tag.length();
		style.setSpan(new ForegroundColorSpan(colorId),fstart,fend,Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
		return style;
	}
}
