package com.lsq.coupon.utils;

import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;

/**
 * Created by lsq on 2017/6/12.
 */

public class FragmentUtil {


    public static void replaceFragmentToActivity(@IdRes int containerId, Fragment fragment, FragmentManager fragmentManager) {
        replaceFragmentToActivity(containerId, fragment, fragmentManager, null);
    }

    public static void replaceFragmentToActivity(@IdRes int containerId, Fragment fragment, FragmentManager fragmentManager, String tag) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_NONE);
        if (TextUtils.isEmpty(tag))
            ft.replace(containerId, fragment);
        else {
            ft.replace(containerId, fragment, tag);
            ft.addToBackStack(tag);
        }
        ft.commitAllowingStateLoss();
    }

    public static boolean popBackStack(FragmentManager fragmentManager) {
        return popBackStack(fragmentManager, null);
    }

    public static boolean popBackStack(FragmentManager fragmentManager, String tag) {
        boolean isBackSuccess = false;
        if (fragmentManager.getBackStackEntryCount() > 0) {
            if (TextUtils.isEmpty(tag))
                fragmentManager.popBackStack();
            else
                fragmentManager.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            isBackSuccess = true;
        }
        return isBackSuccess;
    }

    public static void clearBackStack(FragmentManager fragmentManager) {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

}
