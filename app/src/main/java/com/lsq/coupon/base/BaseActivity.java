package com.lsq.coupon.base;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.lsq.coupon.R;
import com.lsq.coupon.utils.FragmentUtil;
import com.lsq.coupon.utils.ToastUtils;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Morphine on 2017/6/12.
 */

public abstract class BaseActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks, BackHandledInterface {


    protected String TAG = this.getClass().getSimpleName();
    protected Toolbar toolbar;
    protected Unbinder unbinder;
    protected CompositeDisposable compositeDisposable;
    protected TextView toolbarTitle;
    long firstTime;
    private BaseFragment baseFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeDisposable = new CompositeDisposable();
        setContentView(getLayoutResId());
        unbinder = ButterKnife.bind(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ActivityManager.getInstance().push(this);
        if (MyApplication.flag == -1) {
//            ActivityManager.getInstance().toSplashActivity();
        }
        init(savedInstanceState);
        toolbar = (Toolbar) findViewById(getToolBarId());
        if (toolbar != null) {
            toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            if (toolbarTitle != null) {
                toolbarTitle.setText(getPagerTitle());
            }
        }
        initData();
    }

    protected String getPagerTitle() {
        return " ";
    }

    @LayoutRes
    protected abstract int getLayoutResId();

    protected abstract void init(Bundle savedInstanceState);

    protected abstract void initData();

    @Override
    public void setSelectedFragment(BaseFragment selectedFragment) {
        this.baseFragment = selectedFragment;
    }

    @IdRes
    public int getToolBarId() {
        return R.id.toolbar;
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (baseFragment == null || !baseFragment.onBackPressed()) {
            if (FragmentUtil.popBackStack(getSupportFragmentManager())) return true;
            ActivityManager.getInstance().popActivity(this);
        }
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        if (baseFragment == null || !baseFragment.onBackPressed()) {
            updatePagerTitle();
            if (FragmentUtil.popBackStack(getSupportFragmentManager())) return;
            if (ActivityManager.getInstance().currentActivitySize() == 1) {
                exitApp();
            } else {
                ActivityManager.getInstance().popActivity(this);
            }
        }
    }


    private void exitApp() {
        long secondTime = System.currentTimeMillis();
        if (secondTime - firstTime > 2000) {
            ToastUtils.showToast("再按一次退出程序");
            firstTime = secondTime;
        } else {
            ActivityManager.getInstance().exitApp();
        }
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) unbinder.unbind();
        compositeDisposable.clear();
        ActivityManager.getInstance().popActivity(this);
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }


    protected void updatePagerTitle() {
        if (toolbarTitle == null) return;
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count > 1) {
            FragmentManager.BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(count - 1);
            toolbarTitle.setText(entry.getName());
        } else {
            toolbarTitle.setText(getPagerTitle());
        }
    }

    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        Configuration config = new Configuration();
        config.setToDefaults();
        res.updateConfiguration(config, res.getDisplayMetrics());
        return res;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (int i = 0; i < fragments.size(); i++) {
                if (fragments.get(i) != null)
                    fragments.get(i).onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}
