package com.lsq.coupon.base;

import android.support.multidex.MultiDexApplication;

import com.lsq.coupon.utils.PreferencesUtil;
import com.uuzuche.lib_zxing.activity.ZXingLibrary;


/**
 * Created by admin on 2016/11/9.
 * 应用入口全局类
 */
public class MyApplication extends MultiDexApplication {
    /**
     * 判断被系统杀死  默认-1
     */
    public static int flag = -1;
    public ActivityManager activityManager;
    private static MyApplication instance;
    public PreferencesUtil preferencesUtil;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        activityManager = ActivityManager.getInstance();

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
        ZXingLibrary.initDisplayOpinion(getApplicationContext());
        preferencesUtil = new PreferencesUtil(instance);
        preferencesUtil.setName(Constants.PRIVACE_NAME);
        preferencesUtil.init();
//            }
//        }).start();
    }

    public static MyApplication getInstance() {
        // 因为我们程序运行后，Application是首先初始化的，如果在这里不用判断instance是否为空
        return instance;
    }

    @Override
    public void onTerminate() {
        // 程序终止的时候执行
        super.onTerminate();
    }

}