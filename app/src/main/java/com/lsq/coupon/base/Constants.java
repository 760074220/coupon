package com.lsq.coupon.base;

/**
 * Created by lishuqi on 2018/1/31.
 */

public class Constants {
    //文件名
    public static final String PRIVACE_NAME = "qdm";
    /**
     * 定位权限
     */
    public static final int REQUEST_LOCATION_PERMISSIONS = 12;
    /**
     * 拍照权限
     */
    public static final int REQUEST_PHOTO_PERMISSIONS = 1;

    /**
     * 请求存储卡权限请求码
     */
    public static final int REQUEST_SDCARD_PERMISSIONS = 1;
}
