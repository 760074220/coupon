package com.lsq.coupon.base;


import android.text.TextUtils;

import com.lsq.coupon.data.UserInfoBean;
import com.lsq.coupon.utils.PreferencesUtil;
import com.lsq.coupon.utils.StringUtils;


/**
 * @author lishuqi
 * @date 2018/2/13
 */

public class UserManager {
    private static UserManager userManager;

    private UserManager() {
    }

    public static UserManager getInstance() {
        if (userManager == null) {
            userManager = new UserManager();
        }
        return userManager;
    }

    /**
     * accessToken
     */
    public String accessToken;
    /**
     * role
     */
    public int role;
    /**
     * 用户信息
     */
    public UserInfoBean userInfoBean;

    /**
     * 忽略版本
     */
    public String IgnoreVersion;

    /**
     * 是否忽略版本 1忽略 0不忽略
     */
    public String isIgnoreVersion;


    /**
     * 判断用户是否登录
     */
    public boolean isLogin() {
        if (StringUtils.isEmpty(accessToken)) {
            String tokenId = PreferencesUtil.getString(PreferenConstans.ACCESSTOKEN);
            if (TextUtils.isEmpty(tokenId) || TextUtils.equals(tokenId, "")) {
                return false;
            }
            accessToken = tokenId;
            return true;
        }
        return true;
    }

    public String getAccessToken() {
        if (accessToken == null) {
            accessToken = PreferencesUtil.getString(PreferenConstans.ACCESSTOKEN);
        }
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        PreferencesUtil.putString(PreferenConstans.ACCESSTOKEN, accessToken);
        this.accessToken = accessToken;
    }

    public int getRole() {
        if (role == 0) {
            role = PreferencesUtil.getInt(PreferenConstans.ROLE);
        }
        return role;
    }

    public void setRole(int role) {
        PreferencesUtil.putInt(PreferenConstans.ROLE, role);
        this.role = role;
    }

    public UserInfoBean getUserInfoBean() {
        if (userInfoBean == null) {
            userInfoBean = PreferencesUtil.getObject(PreferenConstans.USERINFO, UserInfoBean.class);
        }
        return userInfoBean;
    }

    public void setUserInfoBean(UserInfoBean userInfoBean) {
        PreferencesUtil.putObject(PreferenConstans.USERINFO, userInfoBean);
        this.userInfoBean = userInfoBean;
    }

    public String getIgnoreVersion() {
        if (TextUtils.isEmpty(IgnoreVersion)) {
            IgnoreVersion = PreferencesUtil.getString(PreferenConstans.VERSION);
        }
        return IgnoreVersion;
    }

    public void setIgnoreVersion(String ignoreVersion) {
        PreferencesUtil.putString(PreferenConstans.VERSION, ignoreVersion);
        this.IgnoreVersion = ignoreVersion;
    }

    public String getIsIgnoreVersion() {
        if (TextUtils.isEmpty(isIgnoreVersion)) {
            isIgnoreVersion = PreferencesUtil.getString(PreferenConstans.ISIGNORE);
        }
        return isIgnoreVersion;
    }

    public void setIsIgnoreVersion(String isIgnoreVersion) {
        PreferencesUtil.putString(PreferenConstans.ISIGNORE, isIgnoreVersion);
        this.isIgnoreVersion = isIgnoreVersion;
    }
}
