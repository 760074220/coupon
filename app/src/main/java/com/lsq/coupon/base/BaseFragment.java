package com.lsq.coupon.base;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.lsq.coupon.R;

import java.lang.reflect.Field;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Morphine on 2017/6/12.
 */

public abstract class BaseFragment extends Fragment implements EasyPermissions.PermissionCallbacks {

    protected CompositeDisposable compositeDisposable;
    protected Context context;
    protected View root;
    protected static final int LIMIT = 20;
    private boolean isLazy = false;
    protected BackHandledInterface mBackHandledInterface;
    protected Unbinder unbinder;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    protected void setPagerTitle(String pagerTitle) {
        TextView toolBarTitle = (TextView) getActivity().findViewById(R.id.toolbar_title);
        if (toolBarTitle != null) toolBarTitle.setText(pagerTitle);
    }

    protected Toolbar getToolbar() {
        return null;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBackHandledInterface = (BackHandledInterface) getActivity();
        compositeDisposable = new CompositeDisposable();
    }

    protected boolean onBackPressed() {
        return false;
    }

    @Override
    public void onStart() {
        mBackHandledInterface.setSelectedFragment(this);
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(getLayoutRes(), container, false);
        unbinder = ButterKnife.bind(this, root);
        init(savedInstanceState);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        Toolbar toolbar = getToolbar();
        if (toolbar != null) {
            activity.setSupportActionBar(toolbar);
            activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        initData();
        return root;
    }

    public String getPagerTitle() {
        return "";
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (root != null && isVisibleToUser && !isLazy) {
            initData();
        }
    }

    protected void setLazy(boolean isLazy) {
        this.isLazy = isLazy;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field declaredField = Fragment.class.getDeclaredField("mChildFragmentManager");
            declaredField.setAccessible(true);
            declaredField.set(this, null);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    protected abstract void initData();

    protected abstract void init(Bundle savedInstanceState);

    @LayoutRes
    protected abstract int getLayoutRes();


    @Override
    public void onDestroy() {
        unbinder.unbind();
        compositeDisposable.clear();
        super.onDestroy();
    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }
}
