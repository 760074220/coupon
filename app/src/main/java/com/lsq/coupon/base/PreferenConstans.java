package com.lsq.coupon.base;

/**
 * Created by lishuqi on 2018/1/31.
 */

public class PreferenConstans {
    /**
     * 版本号
     */
    public static final String VER = "version";

    /**
     * 设备串
     */
    public static final String UUID = "uuid";

    /**
     * 手机型号
     */
    public static final String PHONE = "phoneType";

    /**
     * 系统版本
     */
    public static final String SYS = "sysVersion";

    /**
     * 第一次打开
     */
    public static final String FRIST = "frist";


    /**
     * accessToken   是否登录
     */
    public static final String ACCESSTOKEN = "accessToken";


    /**
     * 用户信息
     */
    public static final String USERINFO = "userInfo";

    /**
     * 用户角色
     */
    public static final String ROLE = "role";

    /**
     * 存储起来的未接订单数据
     */
    public static final String TASKDATA = "taskData";

    /**
     * 储存文件的时间
     */
    public static final String DATE = "taskDate";

    /**
     * 版本号
     */
    public static final String VERSION = "version";

    /**
     * 是否忽略
     */
    public static final String ISIGNORE = "isIgnore";

}
