package com.lsq.coupon.data;

/**
 * Created by lsq on 2019/2/11.
 */

public class UserFragmentBean {
    private int id;
    private String neme;
    private int img;

    public UserFragmentBean(int id, String neme, int img) {
        this.id = id;
        this.neme = neme;
        this.img = img;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNeme() {
        return neme;
    }

    public void setNeme(String neme) {
        this.neme = neme;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
