package com.lsq.coupon.data;

/**
 * Created by lsq on 2019/2/28.
 */

public class HomeTabBean {
    private String title;
    private int img;

    public HomeTabBean(String title, int img) {
        this.title = title;
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
