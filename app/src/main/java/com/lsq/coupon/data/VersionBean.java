package com.lsq.coupon.data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Morphine on 2017/10/24.
 */

public class VersionBean implements Serializable {

    private static final long serialVersionUID = -4911237936158591633L;


    public String apkPath;
    public String message;
    public int curVersionCode;

    public String versionType;
    public String apkPackage;
    public String productLanguage;
    public String apkCode;
    public String systemRequire;
    public String softname;
    public String apkSize;
    public String versionName;
    public String updateContent;
    public String platform;

    private int id;
    private int type;
    private String version;
    private String updateTime;
    private String packageUrl;
    private String smallIconUrl;
    private String fullIconUrl;
    private String plistUrl;

    /**
     * 强制更新 0否 1是
     */
    private Integer forceUpdate;

    private List<String> descriptionList;

    private Integer versionNum;

    public Integer getVersionNum() {
        return versionNum;
    }

    public void setVersionNum(Integer versionNum) {
        this.versionNum = versionNum;
    }

    public List<String> getDescriptionList() {
        return descriptionList;
    }

    public void setDescriptionList(List<String> descriptionList) {
        this.descriptionList = descriptionList;
    }

    public Integer getForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(Integer forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getPackageUrl() {
        return packageUrl;
    }

    public void setPackageUrl(String packageUrl) {
        this.packageUrl = packageUrl;
    }

    public String getSmallIconUrl() {
        return smallIconUrl;
    }

    public void setSmallIconUrl(String smallIconUrl) {
        this.smallIconUrl = smallIconUrl;
    }

    public String getFullIconUrl() {
        return fullIconUrl;
    }

    public void setFullIconUrl(String fullIconUrl) {
        this.fullIconUrl = fullIconUrl;
    }

    public String getPlistUrl() {
        return plistUrl;
    }

    public void setPlistUrl(String plistUrl) {
        this.plistUrl = plistUrl;
    }
}
